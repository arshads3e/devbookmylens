<script src="<?php echo base_url(); ?>assets/dist/js/bootstrap.min.js?<?php echo version; ?>"></script>
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- modernizr -->
  <script src="<?php echo base_url(); ?>assets/dist/js/modernizr.js?<?php echo version; ?>" type="text/javascript"></script>
  <!-- datepicker -->
  <script src="<?php echo base_url(); ?>assets/dist/plugins/datepicker/bootstrap-datepicker.js?<?php echo version; ?>" type="text/javascript"></script>
  <!-- Slimscroll -->
  <script src="<?php echo base_url(); ?>assets/dist/plugins/slimScroll/jquery.slimscroll.min.js?<?php echo version; ?>" type="text/javascript"></script>
  <!-- FastClick -->
  <script src='<?php echo base_url(); ?>assets/dist/plugins/fastclick/fastclick.min.js?<?php echo version; ?>'></script>
  <script src='<?php echo base_url(); ?>assets/dist/js/adminBML.js?<?php echo version; ?>'></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js?<?php echo version; ?>" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/dist/plugins/datatables/jquery.dataTables.min.js?<?php echo version; ?>" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/dist/plugins/datatables/dataTables.bootstrap.min.js?<?php echo version; ?>" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/dist/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js?<?php echo version; ?>" type="text/javascript"></script>
  
 <script type="text/javascript" src="<?php echo base_url("assets/js/jquery-ui.js?".version); ?>"></script>
 <script type="text/javascript" src="<?php echo base_url("assets/js/bml.js?".version); ?>"></script>
</body>
</html>
