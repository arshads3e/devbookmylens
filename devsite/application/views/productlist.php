<div class="clearfix"></div>
<br>
<div class="col-sm-2 col-md-2"></div>
<div class="col-sm-8 col-md-8 padding-0px">
  <div class="row">
    <div class="col-md-8 col-sm-8">
    <?php
      $breadcrumbText = '';
      foreach ($breadcrumb as $breadcrumbRow){
        $breadcrumbText .= ($breadcrumbRow['link'] != '') ? "<a href = '".$breadcrumbRow['link']."'>".$breadcrumbRow['name']."</a>" :$breadcrumbRow['name'];
        $breadcrumbText .= " > ";
      }
      echo rtrim($breadcrumbText, " > ");
    ?>
    </div>
    
  </div>

  <?php foreach ($productlistArr as $productlistRow) { ?>
        <div class="col-md-4 col-sm-6 padding-0px-5px-0px-5px"  >
            <div class="productimagebox1">
              <div class="product_view" class="wrapper"
                      style="background-image: url('<?php echo site_url("images/viewImages/".$productlistRow['itemImage']); ?>');background-repeat:no-repeat ;">
                <span class="text">
                  <div style="display:inline-block;overflow:hidden;background-image:url('<?php echo site_url('images/viewrentimg.PNG'); ?> ');background-repeat:no-repeat;background-size: 100%;width: 100%;">
                      <a href="<?php echo site_url('product/view/'.bml_urlencode($productlistRow['itemName']));?>" class="border-none">
                        <div  style="display:block;width:195px;height:130px;margin-top:20px;margin-left:-5px;border:none;display:inline-block;width:100px;height:100px;"></div>
                      </a>
                      <a href="<?php echo site_url('product/rent/'.bml_urlencode($productlistRow['itemName']));?>" style="border:none;display:inline-block;width:100px;height:100px;">
                      </a>

                  </div>

                </span>
              </div>
              <div class="product_left_content" style="height:41px;">
                <span class = "float-left show-title width-70" style="font-size: 11px;width: 70%;position: absolute;bottom: 3px;">
                  <?php echo substr($productlistRow['itemName'], 0, 50); ?>
                </span>
                <div class = "float-right show-price" style="line-height:40px;">
					<?php foreach ($productlistRow['priceList'] as $priceListRow) {
						if($priceListRow['days'] == 1) {
							$price = $priceListRow['price'];break;
						}
					} ?>
                  <?php echo $price."&nbsp;INR"; ?>
                </div>
              </div>
              <div class="price makeVisible" style="display: none">
                <span class='onclick'>
                  <div class='priceTable'>
                    <div class='priceTableHeading'>No. of Days</div>
                    <div class='priceTableHeading'>Per Day Rates</div>
                    <?php foreach ($productlistRow['priceList'] as $priceListRow) {?>
                      <div class='priceTableCol'><?php echo $priceListRow['days']."+";?></div>
                      <div class='priceTableCol'>
                        <?php
                          if($priceListRow['price']) {
                            echo $priceListRow['price']."&nbsp;INR";
                          }
                          else {
                            echo "* Contact Us";
                          }
                        ?>
                      </div>
                    <?php } ?>
                  </div>
                </span>
              </div>

            <!-- adding the product description -->
              <div class="proddesc makeVisible" style="display: none">
                <span class='onhover'>
                  <div class="text-justify margin-top-20px font-family-helvetica font-size-12px color-fff margin-0-10px-0-10px overflow-hidden height-100" >
                    <?php echo $productlistRow['itemDescription']; ?>
                  </div>
                </span>
              </div>
            </div>
        </div>
      <?php } ?>

    </div>
<div class="col-sm-2 col-md-2"></div>
