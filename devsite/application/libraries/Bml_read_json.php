<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bml_read_json {

    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('file');
    }

    public function readRentMenu()
    {
      $jsonFileName = FCPATH."json/rentMenu.json";
      $rentMenuArr = (array)json_decode(read_file($jsonFileName));
      return $rentMenuArr;
    }
    public function readFaqs()
    {
      $jsonFileName = FCPATH."json/faqs.json";
      $faq = (array)json_decode(read_file($jsonFileName));
      return $faq;
    }
}
