<?php 



if ( ! defined('BASEPATH')) exit('No direct script access allowed');



if ( session_status() == PHP_SESSION_NONE ) {

  session_start();

}



define('FACEBOOK_SDK_SRC_DIR', APPPATH . 'libraries/facebook/Facebook/');



// Autoload the required files

require_once( APPPATH . 'libraries/facebook/Facebook/autoload.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/FacebookSession.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/FacebookRedirectLoginHelper.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/FacebookRequest.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/FacebookResponse.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/FacebookSDKException.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/FacebookRequestException.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/FacebookAuthorizationException.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/GraphObject.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/GraphUser.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/Entities/AccessToken.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/HttpClients/FacebookHttpable.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/HttpClients/FacebookCurlHttpClient.php' );

require_once( APPPATH . 'libraries/facebook/Facebook/HttpClients/FacebookCurl.php' ); 



use Facebook\FacebookRedirectLoginHelper;

use Facebook\FacebookSession;

use Facebook\FacebookRequest;

use Facebook\GraphUser;





class Facebook {

  public $ci;

  var $helper;

  var $session;

  var $permissions;



  public function __construct() {

    $this->ci =& get_instance();

    $this->permissions = $this->ci->config->item('permissions', 'facebook');



	// Initialize the SDK

	FacebookSession::setDefaultApplication($this->ci->config->item('api_id', 'facebook'), $this->ci->config->item('app_secret', 'facebook'));



    // Create the login helper and replace REDIRECT_URI with your URL

    // Use the same domain you set for the apps 'App Domains'

    // e.g. $helper = new FacebookRedirectLoginHelper( 'http://mydomain.com/redirect' );

    $this->helper = new FacebookRedirectLoginHelper($this->ci->config->item('redirect_url', 'facebook'));



  }



  /**

   * Returns the login URL.

   */

  public function login_url() {

    return $loginUrl = $this->helper->getLoginUrl($this->permissions);

  }



  /**

   * Returns the current user's info as an array.

   */

  public function get_user() {

    $session = false;

	try {

		$session = $this->helper->getSessionFromRedirect();

	} catch(FacebookRequestException $ex) {

		return $ex;

	} catch(\Exception $ex) {

		return $ex;

	}

	

	if ($session) {

		$me = ((new FacebookRequest( $session, 'GET', '/me?fields=id,name,email,first_name,last_name,gender,link'))->execute()->getGraphObject(GraphUser::className()));

		$userData['sessionID'] = $session;

		$userData['id'] = $me->getProperty('id');

		$userData['email'] = $me->getProperty('email');
		
		$userData['first_name'] = $me->getProperty('first_name');

		$userData['gender'] = $me->getProperty('gender');

		$userData['last_name'] = $me->getProperty('last_name');

		$userData['link'] = $me->getProperty('link');

		/*if($userData['email']===NULL){

            //echo "<script>alert('please add email to your facebook account');</script>";

            echo "please add email to your facebook account";

    	    redirect(site_url(''));

		}*/

		$request = new FacebookRequest($session,'GET','/me/picture?type=large&redirect=false');

		$response = $request->execute();

		

		$graphObject = $response->getGraphObject()->asArray();

		$userData['picsURL'] = (array_key_exists('url',$graphObject))? $graphObject['url'] : '';

		

		return $userData;

	}

	return false;

  }

}