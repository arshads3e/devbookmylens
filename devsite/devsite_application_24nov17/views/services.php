<div class="clearfix"></div>
<br>

	
		<div class="col-sm-2 col-md-2"></div>
		<div class="col-sm-8 col-md-8">
			<h3 class="font-family-helvetica font-weight-bold color-c1272c">Services</h3>
<div id="content" class="font-family-helvetica text-justify">
<p><b>Pre-Production</b></p>
<p>Scripting, Storyboarding, Casting & Location Scouting.</p>
<p><b>Production</b></p>
<p>Cinematography, Photography, Direction & Sync Sound.</p>
<p><b>Post-Production</b></p>
<p>Online & Offline Editing, Color Grading(DI), Graphics,Title Design 
Sub-Titling, Sound Design, Voice Over, Transcoding.</p>
<p><b>Studio</b></p>
<p>Voice Over & Dubbing facility for Films & Videos
Sound Mixing & Mastering .</p>
</br>

</br>
		</div>
		
	     <div class="col-sm-2 col-md-2"></div>
