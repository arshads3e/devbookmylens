

<div class="clearfix"></div>
<br>
<div class="col-sm-2 col-md-2"></div>
<div class="col-sm-8 col-md-8 ">
<div class="row">
   <?php if(!$this->user_session->isSessionVarExists('firstTime')) {
      $this->user_session->setSessionVar('firstTime',false);
       ?>
   <div class="col-sm-12 col-md-12 padding-right-0px">
      <!-- Modal -->
      <div id="myModal" class="modal modal-fullscreen force-fullscreen" aria-hidden="false" style="display: none;" role="dialog">
         <div class="margin-60px-auto width-88">
            <!-- Modal content-->
            <div class="modal-content background-color-7F7F7F">
               <div class="modal-body height-378px background-color-C1272D">
                  <div class="top--39px right--29px" style="position:relative">
                     <script>function hideModal() { $('#myModal').css("display","none");}</script>
                     <button onclick="hideModal();" type="button" class="close color-fff font-size-30px" data-dismiss="modal"><span ><i class="fa fa-times-circle"></i></span></button>
                  </div>
                  <div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                     <div class="slides height-350px" data-group="slides">
                        <ul>
                           <?php foreach ($homeSlide as $slide) { ?>
                           <li>
                              <div class="slide-body" data-group="slide">
                                 <img src="<?php echo site_url('images/sliderImages/'.$slide['popupItemImage']);?>" class="img-responsive">
                                 <div class="caption header" data-animate="slideAppearRightToLeft" data-delay="200" data-length="300">
                                    <h4><?php echo $slide['popupTitle'];?></h4>
                                 </div>
                                 <div class="caption img1" data-animate="slideAppearLeftToRight" data-delay="400" data-length="300">
                                    <img src="<?php echo site_url('images/sliderImages/'.$slide['popupImage']);?>" width="100px" height="100px" >
                                 </div>
                                 <div class="caption text1" data-animate="slideAppearRightToLeft" data-delay="700" data-length="300">
                                    <?php 
                                       $string=$slide['popupDesc'];
                                       ?>
                                    <p> <?php echo wordwrap($string,70,"<br>\n"); ?> </p>
                                    <p><strong>Read Complete Review</strong></p>
                                    <a href="#" class="button font-weight-bold font-size-14px width-140px color-fff background-EAD608 margin-left-0px height-28px padding-5px-5px"> Book This Lens</a>
                                 </div>
                              </div>
                           </li>
                           <?php } ?>                     
                        </ul>
                     </div>
                     <a class="slider-control left" href="#" data-jump="prev"><i class="fa fa-chevron-circle-left"></i></a>
                     <a class="slider-control right" href="#" data-jump="next"><i class="fa fa-chevron-circle-right"></i></a>
                     <div class="pages">
                        <?php $i=0; 
                           foreach ($homeSlide as $slide) { 
                             ++$i?>
                        <a class="page " href="#" data-jump-to="<?php echo $i; ?>">1</a>
                        <?php } ?>
                     </div>
                  </div>
               </div>
               <div class="modal-footer padding-0px text-align-center border-top-none margin-top-15px background-c1272d">
                  <div class="col-sm-2 col-lg-2 padding-top-2">
                     <h4><b class="color-fff padding-25-0px">LATEST ADDITIONS</b></h4>
                  </div>
                  <div class="col-sm-10 col-lg-10">
                     <ul id="flexiselDemo3">
                        <?php foreach ($tableRows as $col) {  ?>
                        <?php foreach ($itemResult as $row) { ?>
                        <?php if($col['itemId']==$row['itemId'])
                           { 
                             $string= $row['itemName'];
                             ?>
                        <li>
                           <a href="<?php echo site_url('product/view/'.bml_urlencode($row['itemName']));?>">
                           <img width="150px" src="<?php echo site_url('images/viewImages/'.$row['itemImage']); ?>" />
                           </a>
                           <p> <?php 
                              $string = (strlen($string) > 25) ? substr($string,0,20).'...' : $string;
                              
                              echo $string;  ?> </p>
                        </li>
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php } ?>
   <?php foreach ($mainPageCategory as $mainPageCategoryRow) { ?>
   <?php if ($mainPageCategoryRow['width'] == 1): ?>
   <div class="col-md-3 col-sm-4 col-xs-12 padding-right-0px">
      <?php else: ?>
      <div class="col-md-6 col-sm-8 col-xs-12 padding-right-0px">
         <?php endif ?>
         <div class="category_design"
            style="background-image: url('<?php echo site_url('images/'.$mainPageCategoryRow['categoryImage'].'.png');?>'); background-repeat: no-repeat;  background-color: #<?php echo $mainPageCategoryRow['bgcolor'];?>;background-position: center;background-size: 95%;">
            <p id="contentdesign">
               <?php echo $mainPageCategoryRow['categoryTitle']; ?>
            </p>
            <div class="imagebox1-hover" >
               <a href="<?php echo site_url('productlist/category/'.urlencode($mainPageCategoryRow['categoryNames']));?>">
                  <div class="top5content" style="background-color: #<?php echo $mainPageCategoryRow['bgcolor'];?>;">
                     <h2 class="top5heading">
                        <?php echo $mainPageCategoryRow['categorySubTitle']; ?>
                     </h2>
                     <ul class="top5">
                        <?php foreach(explode(',',$mainPageCategoryRow['top5Products']) as $row){?>
                        <li><?php echo $row; ?></li>
                        <?php } ?>
                     </ul>
                     <image src="<?php echo site_url('images/'.$mainPageCategoryRow['categoryThumbImage'].'.png');?>" width="75px"
                        class="imgThumbnail">
                  </div>
               </a>
            </div>
         </div>
      </div>
      <?php } ?>
   </div>
</div>
<div class="col-sm-2 col-md-2"></div>
<div id="my-modal" class="modal fade in" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content" id="forgot" style="display:none;">
         <div class="modal-header background-color-fff border-bottom-none" >
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
         </div>
         <div class="modal-body background-color-fff" >
            <div class="col-sm-3 col-md-3"></div>
            <div class="col-md-6 col-sm-6" >
               <form name="forgotPassForm" action="<?php echo site_url('myaccount/forgotPassword');?>" id="forgotPassForm" class="form-inline" method="post" role="form" >
                  <div id="checkheader"></div>
                  <div id="logintoForgotpwd"></div>
                  <div class="row" >
                     <h5 class="fontDesign text-center">Forgot Password</h5>
                     <div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">
                        <input type="email" required name="email" id="email" class="form-control bordervalue width-100" placeholder="Enter your email-id" value=""  >
                     </div>
                     <div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">
                        <input type="submit" name="SubmitLogin" class="btn btn-danger btn-md width-100" value="Reset Password">
                     </div>
                     <div  class="formAlign " >
                        <a href="#login" class="dropdown-toggle" data-toggle="modal"><label class="forgotDesign margin-top-15px"   data-dismiss="modal">Back to Login</label></a>
                     </div>
                  </div>
               </form>
            </div>
            <div class="col-sm-3 col-md-3"></div>
            <div class="clearfix"></div>
         </div>
         <!-- end login with fb form -->
      </div>
      <div class="modal-content" id="login">
         <div class="modal-body">
                     <div class="row">
               <h4 class="text-center">New user? Select location to create your account</h4>
               <div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">
                  <!-- <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Select Location
                     <span class="caret"></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <?php //foreach ($locationName as $locationNameRow) { ?>
                        <li><a href="<?php //echo site_url(lcfirst($locationNameRow['locationName']));?>">
                           <?php //echo $locationNameRow['locationName'];?>
                           </a>
                        </li>
                        
                     </ul> -->
                  <?php foreach ($locationName as $locationNameRow) { 
                     if($locationNameRow['locationName']=="Bengaluru")
                      $align_button="pull-right";
                      else
                      $align_button="pull-left";
                      ?>
                  <div class="<?php echo $align_button;?>">
                     <a href="<?php echo site_url(lcfirst($locationNameRow['locationName']));?>">
                     <button type="button" class="btn btn-info">
                     <?php echo $locationNameRow['locationName'];?>
                     </button>
                     </a>
                  </div>
                  <?php } ?>
               </div>
            </div>
            <hr>
            <div class="row" style="background-color:white;">
               <div class="Absolute-Center is-Responsive">
                  <div id="logo-container"></div>
                  <div class="col-sm-12 col-md-10 col-md-offset-1">
                     <a href="#signupModal" class="dropdown-toggle color-fff" data-toggle="modal" >Log In/Sign Up </a>
                     <p class="statusMsg"></p>
                     <form action="<?php echo site_url('myaccount/login');?>" id ="loginForm1" name="loginForm"  method="post" role="form" >
                        <h4 class="text-center">Already have an account? Sign in </h4>
                        <div class="form-group input-group">
                           <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                           <input class="form-control" id="inputEmail" type="text" name='username' placeholder="username"/>     
                        </div>
                        <div class="form-group input-group">
                           <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                           <input class="form-control" type="password" id="inputPassword" name='password' placeholder="password"/>  
                        </div>
                        <div class="form-group">                     
                           <input type="submit" name="SubmitLogin" class="btn btn-danger btn-md width-100" value="Login"  >
                        </div>
                        <div class="form-group text-center">
                           <a href="#forgotpassword" class="dropdown-toggle" data-toggle="modal"><label class="forgotDesign2"  data-dismiss="modal">Forgot Password</label></a>
                        </div>
                     </form>
              
                  </div>
               </div>
            </div>

         </div>
      </div>
   </div>
</div>
<style>
   .modal {
   display:block;
   }
</style>
<div class="modal-backdrop fade in"></div>
<script type="text/javascript">
   $(window).load(function() {
      $("#flexiselDemo3").flexisel({
           visibleItems: 4,
           animationSpeed: 1000,
           autoPlay: true,
           autoPlaySpeed: 3000,            
           pauseOnHover: true,
           enableResponsiveBreakpoints: true,
           responsiveBreakpoints: { 
               portrait: { 
                   changePoint:480,
                   visibleItems: 1
               }, 
               landscape: { 
                   changePoint:640,
                   visibleItems: 2
               },
               tablet: { 
                   changePoint:768,
                   visibleItems: 3
               }
           }
       });
   });
</script>
<script>
   $(window).on('load',function(){
       $('#login').modal('show');
       
   });
   $('.forgotDesign2').click(function(){
     $('#login').css("display","none");
     $('#forgot').modal('show');
   });
   $('#Back-to-login').click(function(){
     $('#forgot').css("display","none")
   });
</script>

