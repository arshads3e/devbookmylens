          <div>
            <div>
              <div class="box">
                <div class="box-body" style="overflow: auto;display: block;height: 600px;">
				  <input type="text" id="search"/>
                  <table id="availList" class="table table-bordered table-striped">
                    <col width="230">
					<col width="auto">
					<thead>
                      <tr>
						<th>*</th>
                        <?php foreach ($header as $headerrow) {?>
                          <th><?php echo dateFormat($headerrow['dates'], '%d %M');?></th>
                        <?php } ?>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($out as $id => $tableRow) {
						if(trim($tableRow[0]['itemName']) == "") continue;
						$show = false;
						$html = "";
						$html = "<tr>";
						$html .= "<td>".$tableRow[0]['itemName']."</td>";
                        foreach ($tableRow as $col) {
                          $qty = $col['numStock'] - $col['usedQty'];
						  if($qty <= 0) $show = true;
						  $html .= "<td onclick=\"show('".$col['subOrderId']."')\" title=\"".$tableRow[0]['itemName']." || ".dateFormat($col['dates'], '%d %M')."\" class=\"".(($qty <=0)?"red" :"green")."\">".($col['numStock'] - $col['usedQty'])."</td>";
                        } 
                      $html .= "</tr>";
					  if($show)
						echo $html;
                    } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div><!-- /.content-wrapper -->
	  
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Order Details</h4>
		  </div>
		  <div class="modal-body">
			<div class="col col-md-12" id="orderDetails">
				
			</div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>
	  <style>
		.red {
			background-color:red;
		}
		.green {
			background-color:green;
		}
	  </style>
<script type="text/javascript">

	function show($subOrderId){
		console.log($subOrderId);
		if($subOrderId == "")
		{
			alert("No Orders");
		}
		else
		{
			$.getJSON("<?php echo site_url('admin/avail/details');?>/"+$subOrderId, function(result){
				console.log(result);
				var content = "";
				jQuery.each(result.orderDetails, function($id, $row){
					content += "<table class='table'><thead><tr><th>Name</th><th>OrderNumber</th><th>Start Date</th><th>End Date</th><th>Status</th></tr></thead>";
					content += "<tbody><tr><td>"+$row['details'].customerName+"</td><td>"+$row['details'].orderNumber+"</td><td>"+$row['details'].startDate+"</td><td>"+$row['details'].endDate+"</td><td>"+$row['details'].orderStatusDisplayName+"</td></tr></tbody></table>";
					content += "<table class='table'><thead><tr><th>Name</th><th>Qty</th></tr></thead>";
					content += "<tbody>";
					jQuery.each($row['items'], function($id1, $item){
						content += "<tr><td>"+$item.itemName+"</td><td>"+$item.qty+"</td></tr>";
					});
					content += "</tbody></table>";
				});
				$('#orderDetails').html(content);
				$('#myModal').modal('show'); 
			});
		}
	}
	$("#search").on("keyup", function() {
		var value = $(this).val().toLowerCase();

		$("table#availList tr").each(function(index) {
			if (index !== 0) {

				$row = $(this);

				var id = $row.find("td:first").text().toLowerCase();

				if (id.search(value) === -1) {
					$row.hide();
				}
				else {
					$row.show();
				}
			}
		});
	});
</script>
