 <div>
            <div>
              <div class="box">
                <div class="box-body">
                  
                  <div class="clearfix"></div>
                  <table id="latestDetails" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <td>Popup ID</td>
                        <td>Popup Title</td>
                        <td>Item Name</td>
                        <td>Status</td>
                        <td></td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody>

                      <?php foreach ($tableRows as $col) {  ?>
                        <tr>
                          
                          <?php foreach ($itemResult as $row) { ?>

                          <?php if($col['itemId']==$row['itemId'])
                          { $id= $col['popupId'];?>
                            <td><?php echo $col['popupId'];?></td>
                            <td><?php echo $col['popupTitle'] ;?></td>
                            <td><?php echo $row['itemName'] ;?></td>
                            
                            <td><?php if($row['itemStatus']=='1') {echo "Active" ; } else { echo "InActive" ;  }?></td>
                            <td><a href="<?php echo admin_url('products/viewHomeSlider/'.$id); ?>" class="btn btn-primary btn-xs">View</a></td>
                            <td><a href="<?php echo admin_url('products/removeHomeSlider/'.$id); ?>" class="btn btn-warning btn-xs">Remove</a></td>

                            

                          <?php } ?>
                          <?php } ?>
                          </tr>
                        
                      <?php } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div>
      <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
          $('#latestDetails').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
      </script>

     