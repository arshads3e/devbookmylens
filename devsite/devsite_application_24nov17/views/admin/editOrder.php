<?php $statusButtons = array(
                          "",//no button @index 0
                          '<button name="status" value="APPROVE" class="btn btn-primary ">Approve</button><button name="status" value="SAVE" class="btn btn-primary ">Save</button> <button onclick="return deleletconfig();" class="btn btn-danger " name="status" value="CANCEL">Cancel</button> <a class="btn btn-warning" onclick = "javascript:self.close();" >Close</a>',
                          '<button name="status" value="RENT" class="btn btn-success ">On Rent</button> <button name="status" value="SAVE" class="btn btn-primary ">Save</button> <button class="btn btn-danger" name="status" value="CANCEL" >Cancel</button> <a class="btn btn-warning" onclick = "javascript:self.close();" >Close</a>',
                          '<button name="status" value="RETURNED" class="btn btn-primary ">Returned</button> <button name="status" value="SAVE" class="btn btn-primary ">Save</button> <a class="btn btn-warning" onclick = "javascript:self.close();" >Close</a>',
                          '<button name="status" value="COMPLETE" class="btn btn-success ">completed</button> <button name="status" value="SAVE" class="btn btn-primary ">Save</button> <button name="status" value="CANCEL" class="btn btn-danger ">Cancel</button><a class="btn btn-warning" onclick = "javascript:self.close();" >Close</a>',
                          '<button name="status" value="DELETE" class="btn btn-danger" onclick = "return deleletconfig();" >Delete</button> <a class="btn btn-warning" onclick = "javascript:self.close();" >Close</a>',
                          '<button name="status" value="APPROVE" class="btn btn-primary ">Approve</button> <button name="status" value="SAVE" class="btn btn-primary ">Save</button> <button name="status" value="CANCEL" class="btn btn-danger ">Cancel</button>',
                          '<button name="status" value="CASH" class="btn btn-primary ">Cash</button>',
                          '',
                          '<button name="status" value="APPROVE" class="btn btn-primary ">Approve</button> <button onclick="return deleletconfig();" class="btn btn-danger " name="status" value="CANCEL">Cancel</button> <a class="btn btn-warning" onclick = "javascript:self.close();" >Close</a>'
                        );
?>

<div class="box">
    <div class="box-body">
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="col-lg-6 col-sm-6 text-right">Order Number:</div>
        <div class="col-lg-6 col-sm-6"><b><?php echo $orderDetails['orderNumber'];?></b></div>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-sm-6 text-right">Client Number:</div>
        <div class="col-lg-6 col-sm-6"><?php echo "<a target ='_blank' href = '".admin_url('clients/view/'.$orderDetails['customerNumber'])."'>".$orderDetails['customerNumber']."</a>";?></div>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-sm-6 text-right">Client Name:</div>
        <div class="col-lg-6 col-sm-6"><b><?php echo $orderDetails['customerName'];?></b></div>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-sm-6 text-right">Sub Total:</div>
        <div class="col-lg-6 col-sm-6"><?php echo $orderDetails['orderSubtotal'];?></div>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-sm-6 text-right">Discount Amount:</div>
        <div class="col-lg-6 col-sm-6"><?php echo $orderDetails['orderDiscountAmount'];?></div>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-sm-6 text-right">Total Amount:</div>
        <div class="col-lg-6 col-sm-6"><?php echo $orderDetails['orderTotal'];?></div>
        <div class="col-lg-6 col-sm-6  text-right">Status:</div>
        <div class="col-lg-6 col-sm-6">
          <b><?php echo $orderDetails['orderStatusDisplayName'] ;?></b>
        </div>
        <div class="clearfix"></div>

      </div>
      <form action="<?php echo admin_url('orders/updateStatus')?>" method="post">
			<?php	var_dump($orderDetails['subOrderID']);
			 ?>
      <input type="hidden" name="orderID" value="<?php echo $orderDetails['subOrderID'];?>"  />
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="col-lg-6 col-sm-6 text-right">Collect Date:</div>
        <div class="col-lg-6 col-sm-6"><?php echo dateFromMysqlDate($collectDate);?></div>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-sm-6 text-right">Start Date:</div>
        <div class="col-lg-6 col-sm-6"><?php echo dateFromMysqlDate($startDate);?></div>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-sm-6 text-right">End Date:</div>
        <div class="col-lg-6 col-sm-6"><?php echo dateFromMysqlDate($endDate);?></div>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-sm-6 text-right">Return Date:</div>
        <div class="col-lg-6 col-sm-6"><?php echo dateFromMysqlDate($returnDate);?></div>
        <div class="clearfix"></div>
		<div class="col-lg-offset-7 col-sm-offset-7 col-lg-5 col-sm-5 cursor-pointer">
          <a data-toggle="modal" data-subOrderID ="<?=$orderDetails['subOrderID'];?>" data-target="#datePicktModal" class="btn-transparent fa fa-pencil-square-o text-warning" title="EDIT">Edit</a>
        </div>
		<div class="clearfix"></div>
		<div class="col-lg-4 col-sm-4 text-right">Order Comments:</div>
   	    <div class="col-lg-8 col-sm-8">
		  <textarea name="comments" rows="3" cols="35"><?php echo $orderDetails['comments'];?></textarea>
	    </div>
		
      </div>
      <div class="col-lg-4 col-sm-4 text-center" >
          <div class="col-lg-4 col-sm-4 text-right">Customer Comments:</div>
          <div class="col-lg-8 col-sm-8">
            <textarea name="cust_comments" rows="3" cols="35"><?php echo $orderDetails['cust_comments'];?></textarea>
          </div>
          <div class="clearfix"></div>
		  
		  <div class="col-lg-4 col-sm-4">
		  </div>
		  <div class="col-lg-8 col-sm-8">
          <fieldset class="rating">
			<input type="radio" id="star5" <?php echo ($orderDetails['rating'] == -1 || $orderDetails['rating'] == 5)?"checked":"" ?> name="rating" value="5" /><label for="star5" title="Rocks!">5 stars</label>
			<input type="radio" id="star4"  <?php echo ($orderDetails['rating'] == 4)?"checked":"" ?> name="rating" value="4" /><label for="star4" title="Pretty good">4 stars</label>
			<input type="radio" id="star3"  <?php echo ($orderDetails['rating'] == 3)?"checked":"" ?> name="rating" value="3" /><label for="star3" title="Meh">3 stars</label>
			<input type="radio" id="star2"  <?php echo ($orderDetails['rating'] == 2)?"checked":"" ?> name="rating" value="2" /><label for="star2" title="Kinda bad">2 stars</label>
			<input type="radio" id="star1"  <?php echo ($orderDetails['rating'] == 1)?"checked":"" ?> name="rating" value="1" /><label for="star1" title="Sucks big time">1 star</label>
		  </fieldset>
		  <style>
			.rating {
				float:left;
			}

			/* :not(:checked) is a filter, so that browsers that don’t support :checked don’t 
			   follow these rules. Every browser that supports :checked also supports :not(), so
			   it doesn’t make the test unnecessarily selective */
			.rating:not(:checked) > input {
				position:absolute;
				top:-9999px;
				clip:rect(0,0,0,0);
			}

			.rating:not(:checked) > label {
				float:right;
				width:1em;
				padding:0 .1em;
				overflow:hidden;
				white-space:nowrap;
				cursor:pointer;
				font-size:200%;
				line-height:1.2;
				color:#ddd;
				text-shadow:1px 1px #bbb, 2px 2px #666, .1em .1em .2em rgba(0,0,0,.5);
			}

			.rating:not(:checked) > label:before {
				content: '★ ';
			}

			.rating > input:checked ~ label {
				color: #f70;
				text-shadow:1px 1px #c60, 2px 2px #940, .1em .1em .2em rgba(0,0,0,.5);
			}

			.rating:not(:checked) > label:hover,
			.rating:not(:checked) > label:hover ~ label {
				color: gold;
				text-shadow:1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0,0,0,.5);
			}

			.rating > input:checked + label:hover,
			.rating > input:checked + label:hover ~ label,
			.rating > input:checked ~ label:hover,
			.rating > input:checked ~ label:hover ~ label,
			.rating > label:hover ~ input:checked ~ label {
				color: #ea0;
				text-shadow:1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0,0,0,.5);
			}

			.rating > label:active {
				position:relative;
				top:2px;
				left:2px;
			}
		  </style>
		  </div>
          <div class="clearfix"></div>
		  <?php if($orderDetails['status'] == 2 || $orderDetails['status'] == 3) {?>
		  <div class="col-lg-4 col-sm-4 text-right"></div>
          <div class="col-lg-4 col-sm-4">
            Shipping Cost:
          </div>
		  <div class="col-lg-1 col-sm-1">
			<?php if($orderDetails['status'] == 2) {?>
				<?php if( array_key_exists('pickup', $orderAddress) && $orderAddress['pickup']['addressType'] != 'STORE') {?>
					<input type="text" name="pickup_cost" value="<?php echo $orderAddress['pickup']['shippingCost']; ?>">
				<?php } else { echo "Rs. 0.00"; }?>
			<?php } else if($orderDetails['status'] == 3){ ?>
				<?php if( array_key_exists('delievery', $orderAddress) && $orderAddress['delievery']['addressType'] != 'STORE') {?>
					<input type="text" name="drop_cost" value="<?php echo $orderAddress['delievery']['shippingCost']; ?>">
				<?php } else { echo "Rs. 0.00"; }?>
			<?php } ?>
          </div>
		  <div class="clearfix"></div>
          <?php } ?>
		  <div class="col-lg-8 col-sm-8 col-md-offset-4">
            <?php echo $statusButtons[$orderDetails['status']];?>
          </div>
      </div>
		</form>
      <div class="clearfix"></div>
      <hr>
      <div class="col-lg-4 col-sm-4">
      <h4 class="text-center text-uppercase bg-success margin-top-0px" >Rental Items</h4>
		  <div class="col-lg-12 col-sm-12">
			<div class="col-lg-8 col-sm-8">Item Name</div>
			<div class="col-lg-4 col-sm-4">Item Qty</div>
			<?php $class = (count($orderItems) < 2)?"hide":""; ?>
			<form action="<?php echo admin_url('orders/removeItemFromOrder')?>" method="post" id="deleteItemForm">
				<input type="hidden" name="subOrderID" value="<?= $orderDetails['subOrderID'];?>">
				<?php foreach ($orderItems as $orderItem) {?>
					<div class="col-lg-8 col-sm-8">
						<input class="itemRemoveCheckbox <?= $class;?>" type="checkbox" name="productID[]" value="<?= $orderItem['itemId'];?>">
						<?php echo $orderItem['itemName'];?>
					</div>
					<div class="col-lg-4 col-sm-4">
						<?php echo $orderItem['qty'];?>
					</div>
				<?php } ?>
				<div class="col-lg-12 col-sm-12">
					<input type="submit" value="Delete Selected Products" class="<?= $class;?>">
				</div>
			</form>
		  </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <h4 class="text-center text-uppercase bg-danger margin-top-0px" >Pickup Address</h4>
        <div class="clearfix"></div>
		<?php if(array_key_exists('pickup', $orderAddress)) {?>
        <div class="col-lg-4 col-sm-4 text-right">Address Type:</div>
        <div class="col-lg-8 col-sm-8"><?php echo $orderAddress['pickup']['addressType']; ?></div>
        <div class="clearfix"></div>
        <div class="col-lg-4 col-sm-4 text-right">Address:</div>
        <div class="col-lg-8 col-sm-8">
          <?php echo $orderAddress['pickup']['addressLine1']; ?>,<br><?php echo $orderAddress['pickup']['addressLine2']; ?>,<br><?php echo $orderAddress['pickup']['addressCity']; ?>,<br><?php echo $orderAddress['pickup']['addressState']; ?>,<br><?php echo $orderAddress['pickup']['addressPin']; ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-4 col-sm-4 text-right">Land Mark:</div>
        <div class="col-lg-8 col-sm-8">
          <?php echo $orderAddress['pickup']['addressLandmark']; ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-4 col-sm-4 text-right">Shipping Cost:</div>
        <div class="col-lg-8 col-sm-8"><?php echo $orderAddress['pickup']['shippingCost']; ?></div>
        <div class="clearfix"></div>
		<?php } ?>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <h4 class="text-center text-uppercase bg-info margin-top-0px">Return Address</h4>
        <div class="clearfix"></div>
		<?php if(array_key_exists('delievery', $orderAddress)) {?>
        <div class="col-lg-4 col-sm-4 text-right">Address Type:</div>
        <div class="col-lg-8 col-sm-8"><?php echo $orderAddress['delievery']['addressType']; ?></div>
        <div class="clearfix"></div>
        <div class="col-lg-4 col-sm-4 text-right">Address:</div>
        <div class="col-lg-8 col-sm-8">
          <?php echo $orderAddress['delievery']['addressLine1']; ?>,<br><?php echo $orderAddress['delievery']['addressLine2']; ?>,<br><?php echo $orderAddress['pickup']['addressCity']; ?>,<br><?php echo $orderAddress['pickup']['addressState']; ?>,<br><?php echo $orderAddress['pickup']['addressPin']; ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-4 col-sm-4 text-right">Land Mark:</div>
        <div class="col-lg-8 col-sm-8">
          <?php echo $orderAddress['delievery']['addressLandmark']; ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-4 col-sm-4 text-right">Shipping Cost:</div>
        <div class="col-lg-8 col-sm-8"><?php echo $orderAddress['delievery']['shippingCost']; ?></div>
        <div class="clearfix"></div>
		<?php } ?>

      </div>
    </div>
  </div>

<div id="datePicktModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="height: 280px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Order Dates</h4>
      </div>
      <div class="modal-body">
		<p>Start Date</p>
		<p><input type="text" id="startDateInput" class="datepickr" name="startDate" value="<?=dateFromMysqlDate($startDate);?>"/></p>
        <p>End Date</p>
		<p><input type="text" id="endDateInput" class="datepickr" name="startDate" value="<?=dateFromMysqlDate($endDate);?>"/></p>
		<p><input type="hidden" id="subOrderIDInput" name="subOrderID" value="<?=$orderDetails['subOrderID'];?>"/></p>
		<p><span id="error" class="text-danger"></span></p>
		<p><button type="button" onclick="return formSubmit();">submit</button></p>
	  </div>
    </div>

  </div>
</div>

<script>

  $(function(){
	$( ".datepickr" ).datepicker( {"dateFormat": "dd-M-yy"} );
	$("#deleteItemForm").submit(function(e)
	{
		if($('.itemRemoveCheckbox:checked').length == $('.itemRemoveCheckbox').length)
		{
			alert('You cant delete all Items of an order.\n Instead, Cancel whole order');
			e.preventDefault();
			return;
		}
		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");
		$.post(formURL,postData,
			function (data) {
				console.log(data);
				if(data.status == 'error')
				{
					alert(data.message);
				}
				else
				{
					alert("Item/s deleted");
					location.reload();
				}
			}, "json"
		  );
		e.preventDefault();
	});
  });
  
  function formSubmit()
  {
	$.post('<?php echo admin_url('orders/updateOrderDates/'); ?>',{startDate:$('#startDateInput').val(), endDate:$('#endDateInput').val(), subOrderID:$('#subOrderIDInput').val() },
					function (data) {
						console.log(data);
						if(data.status == 'error')
						{
							$('span#error').text(data.message);
						}
						else
						{
							alert("Date change Succeed");
							$('#datePicktModal').modal('toggle');
							location.reload();
						}
					}, "json"
				  );
  }
  
  function deleletconfig(){
    var del=confirm("Are you sure you want to delete this record?");
    if (del==true){
       alert ("record deleted")
    }
    return del;
  }
  
</script>
