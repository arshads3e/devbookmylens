<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <div>
            <div>
              <div class="box">
                <div class="box-body">
                  <form action="<?php echo admin_url('products/insertProductDetails') ;?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-lg-6 col-sm-6">
                            <div class="col-lg-4 col-sm-4 text-right padding-top1"><b>Item Type :</b></div>
                            <div class="col-lg-7 col-sm-7 padding-top1">
                              <select class="form-control" name='itemType'>
                                <option value="-1">Select</option>
                              <?php foreach ($itemType as $item) { ?>
                                    <option value="<?php echo $item['itemTypeID']; ?>">
                                      <?php echo $item['itemTypeName']; ?>
                                    </option>

                              <?php } ?>
                              </select>
                            </div>
                            <div class="col-lg-1 col-sm-1 padding-top1 padding-opx">
                                <a href="#" data-toggle="modal" data-target="#itemTypeModal"> <i class="fa fa-plus-square-o text-warning" data-toggle="tooltip"  title="ADD ITEM TYPE NAME"></i> </a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-4 col-sm-4 text-right padding-top1">
                              <b>Brand Name :</b>
                            </div>
                            <div class="col-lg-7 col-sm-7 padding-top1">
                              <select class="form-control" name='itemBrand'>
                                <option value="-1">Select</option>
                              <?php foreach ($brandName as $brand) { ?>

                                    <option value="<?php echo $brand['itemBrandID']; ?>">
                                      <?php echo $brand['itemBrandName']; ?>
                                    </option>

                              <?php } ?>
                              </select>
                            </div>
                            <div class="col-lg-1 col-sm-1 padding-top1 padding-opx">
                                <a href="#" data-toggle="modal" data-target="#brandModal"> <i class="fa fa-plus-square-o text-warning" data-toggle="tooltip"  title="ADD BRAND NAME"></i> </a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-4 col-sm-4 text-right padding-top1">
                              <b>Category Name :</b>
                            </div>
                            <div class="col-lg-7 col-sm-7 padding-top1">
                              <select class="form-control" name='category'>
                                <option value="-1">Select</option>
                              <?php foreach ($categoryName as $category) { ?>

                                    <option value="<?php echo $category['categoryID']; ?>">
                                      <?php echo $category['category_Name']; ?>
                                    </option>

                              <?php } ?>
                              </select>
                            </div>
                            <div class="col-lg-1 col-sm-1 padding-top1 padding-opx">
                                <a href="#" data-toggle="modal" data-target="#categoryModal"> <i class="fa fa-plus-square-o text-warning" data-toggle="tooltip"  title="ADD CATEGORY NAME"></i> </a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-4 col-sm-4 text-right padding-top1">
                              <b>Item Name :</b>
                            </div>
                            <div class="col-lg-7 col-sm-7 padding-top1">
                              <input type='text' name='itemname' value="" class="form-control">
                             </div>
                            <div class="col-lg-1 col-sm-1 padding-top1">
                            </div>
                             <div class="clearfix"></div>
                            <div class="col-lg-4 col-sm-4 text-right padding-top1">
                           
                              <b>Select available Locations</b>
                            </div>
                            <div class="col-lg-7 col-sm-7 padding-top1">
                            <?php foreach ($location as $name) { ?>
                              <input type='checkbox' value='Bengaluru' id='<?php echo $name['locationName']; ?>' name='Bengaluru' class='Bengaluru' />
                              <label for='<?php echo $name['locationName']; ?>'><?php echo $name['locationName']; ?></label>
                              <input name ='<?php echo $name['locationName']; ?>-place' id='<?php echo $name['locationName']; ?>place' placeholder='please enter Qty' type='number' disabled /> <br/>   
                              <input type='hidden' value='<?php echo $name['locationKey']; ?>' name='<?php echo $name['locationName']; ?>-key' />
                                <?php } ?>  
                              
                                 </div>
                            <div class="col-lg-1 col-sm-1 padding-top1">
                            </div>
                            <div class="clearfix"></div>
                           
                            <div class="clearfix"></div>

                            <div class="col-lg-4 col-sm-4 text-right padding-top1">
                              <b>Item Status :</b>
                            </div>
                            <div class="col-lg-7 col-sm-7 padding-top1">
                              <select class="form-control" name='itemstatus'>
                                <option value='-1'>Select</option>
                                <option value='1'>Active</option>
                                <option value='0'>InActive</option>
                              </select>
                            </div>
                            <div class="col-lg-1 col-sm-1 padding-top1">
                            </div>
                            <div class="clearfix"></div>
                      </div>
                      <div class="col-lg-6 col-sm-6">
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Item Desciption :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <textarea type='text' name='itemDescription' rows='3' class="form-control"></textarea>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Item Spacification :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <textarea type='text' name='itemSpecifi' rows='3' class="form-control"></textarea>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Item Image (270*180):</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <input type='file' name='itemImage1'  class="form-control" />
                        </div>
						<div class="clearfix"></div>
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Item Image (375*250):</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <input type='file' name='itemImage2'  class="form-control" />
                        </div>
                        <div class="clearfix"></div>

                      </div>
                      <div class="clearfix"></div>
                      <hr>
                      <h1 class="text-center"><button class="btn btn-primary" type="submit">SAVE</button> <button class="btn btn-warning" type="reset">RESET</button>  <a href="" onclick='return deleletconfig();' class="btn btn-danger" >DELETE</a></h2>
                   </form>
                   <div class="clearfix"></div>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div>


      <!-- Modal for add item type -->
<div class="modal fade" id="itemTypeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ADD NEW ITEMTYPE NAME</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo admin_url('products/insertItemType') ;?>" method="post">
          <div class="col-lg-6">
            <input type="text" name="itemtypename" placeholder='Enter type name' class="form-control" required>
          </div>
          <div class="col-lg-6"><button type='submit' class="btn btn-primary"> Submit </button></div>
        </form>
        <div class="clearfix"></div>
      </div>

    </div>
  </div>
</div>

<!-- Modal for add item brand name-->
<div class="modal fade" id="brandModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ADD NEW BRAND NAME</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo admin_url('products/insertBrandType') ;?>" method="post">
          <div class="col-lg-6">
            <input type="text" name="itembrandname" placeholder='Enter brand name' class="form-control" required>
          </div>
          <div class="col-lg-6"><button type='submit' class="btn btn-primary"> Submit </button></div>
        </form>
        <div class="clearfix"></div>
      </div>

    </div>
  </div>
</div>

<!-- Modal for add item category name -->
<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ADD NEW CATEGORY</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo admin_url('products/insertCategoryType') ;?>" method="post">
          <div class="col-lg-6">
            <input type="text" name="itemcategoryname" placeholder='Enter category name' class="form-control" required>
          </div>
          <div class="col-lg-3">
            <input type="text" name="categoryorder" placeholder='Enter order Number' class="form-control" required>
          </div>
          <div class="col-lg-3"><button type='submit' class="btn btn-primary"> Submit </button></div>
        </form>
        <div class="clearfix"></div>
      </div>

    </div>
  </div>
</div>
<script>
$(".location").change(function () {
    //check if its checked. If checked move inside and check for others value
    if (this.checked && this.value === "Bengaluru") {
        //show a text box
        $("#Mysuru").show();
    } else if (!this.checked && this.value === "Bengaluru"){
        //hide the text box
        $("#Mysuru").hide();
    }
  });
 </script>

