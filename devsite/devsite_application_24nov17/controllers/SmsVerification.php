<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class smsVerification extends CI_Controller{
public function index()
{
        if(!$this->user_session->isLoggedIn())
        {
                if($this->user_session->getSessionVar('email'))
                {
                        $this->load->helper(array('form', 'url'));
                        $this->load->view('head');
                        $this->load->view('header');
                        $this->load->view('smsVerification');
                        $this->load->view('footer');
                        $this->load->view('footerDetails');
                }else{
                        redirect(site_url(''));
                }
        }else{
                redirect(site_url(''));
        }
}
public function valid_user_session($value)
{       if ($value === null)
        {
                $this->form_validation->set_message('valid_user_session','Invalid user session');
                return FALSE;                     
        }
        else
        {
                //$this->form_validation->set_message('user_session','Invalid user session');
                return TRUE;
        }
}
public function valid_otp($value)
{
        $emailId=$this->user_session->getSessionVar('email');
        $this->load->model('myaccountModel');
        $otpResult = $this->myaccountModel->getUserOTP($emailId);
        $otpNumber=$otpResult['otpNumber'];
        if ($value != $otpNumber)
        {       $this->form_validation->set_message('valid_otp','Invalid OTP');
                return FALSE;
        }
        else
        {
                return true;
        }
} 
public function checkOptionsSelected(){
    //Getting Email from session
    //$emailId=$this->user_session->getSessionVar('email');
    //$password=$this->user_session->getSessionVar('password');
    /* 
    * Loading form validation library.
    */

    $this->load->library('form_validation');
    $this->form_validation->set_rules('otp','OTP','required|numeric|trim|exact_length[6]|callback_valid_otp');

    $this->form_validation->set_rules('email','E-mail','callback_valid_user_session');

    $this->form_validation->set_error_delimiters("<p class='text-danger'>","<p>");
    // validation
    if($this->form_validation->run()){
    // OTP number
    $entered_otp_number=$this->input->post('otp');
    // Email from session
    $user_email=$this->input->post('email');

    // Loading MyaccountModel
    $this->load->model('myaccountModel');

    // Get User OTP from myaccountModel against email
    $otpResult = $this->myaccountModel->getUserOTP($user_email);

    // OTP from Resulted query
    $otpNumber=$otpResult['otpNumber'];

    // Verify OTP with entered OTP
    if($otpNumber===$entered_otp_number){

    //Get the Details Of User
    $resultSet=$this->myaccountModel->getUserDetails($user_email,$entered_otp_number);

    $fname=$resultSet['firstname'];
    $lname=$resultSet['lastname'];
    $companyPerson=$resultSet['companyPerson'];
    $phone=$resultSet['mobileNumber'];
    $pwd1=$resultSet['password'];
    $emailId=$resultSet['emailId'];
    $gender=$resultSet['gender'];
    $locationKey=$resultSet['locationKey'];

    // Insert into customer table
    $result=$this->myaccountModel->registerUser($companyPerson,$fname,$lname,$gender, $emailId,$pwd1,$phone,$locationKey);
    
    
    // Unique integers
    $code = $result[0][0]['varReferralCode'];
    $customerID = $result[0][0]['customerID'];
    $customerNumber = $result[0][0]['customerNumber'];

    // Loading Email Library  
    $this->load->library("send_email");

    $data['email'] = $emailId;
    $data['firstName'] = $fname;
    $data['code'] = $code;
    $data['customerID'] = $customerID;
    $data['customerNumber'] = $customerNumber;
    $data['locationKey']= $locationKey;
    $this->send_email->sendWelcomeEmail($data);

    $this->loginWithOtp($emailId,$pwd1);
    }else{
    $this->form_validation->set_message('valid_otp', 'Invalid OTP');
    /*  $this->load->view('head');
    $this->load->view('header');
    $this->load->view("smsVerification");
    $this->load->view('footer');
    $this->load->view('footerDetails'); */
    }
    }else{
    $this->load->view('head');
    $this->load->view('header');
    $this->load->view("smsVerification");
    $this->load->view('footer');
    $this->load->view('footerDetails'); 
   // redirect(site_url('smsVerification'));
    }
}
public function loginWithOtp($email,$password){
        $this->load->model('myaccountModel');
        if($status = $this->myaccountModel->validateLogin($email, $password))
        {
        $this->user_session->login($email);
        //$json = $arrayName = array('status' => 'success', "isAdmin" => $this->user_session->getSessionVar('is_admin_login'));
        redirect(site_url('myaccount'));
        }
        else
        {
        $json = $arrayName = array('status' => 'failure');
        }   
}

public function generateOTPNumber()
{       
        // Loding Form Validation library.
        $this->load->library('form_validation');
        // Verifing wheather E-mail is present in Session.
        $this->form_validation->set_rules('email','E-mail','callback_valid_user_session');
        // If valid session 
        if($this->form_validation->run())
        {       
                // Getting E-mail from session.
                $emailId=$this->user_session->getSessionVar('email');
                // Generating random six digit number.
                $otp_number = mt_rand(100000, 999999);
                // Loading MyaccountModel file from Models folder.
                $this->load->model('myaccountModel');
                /**
                * Updating Otp number in tmp_customer table
                * With respect to email.
                */
                $this->myaccountModel->updateOTP($emailId,$otp_number);
                // Get details of user with email and otp number.
                $resultSet=$this->myaccountModel->getUserDetails($emailId,$otp_number); 
                // phone number from database.
                $phone=$resultSet['mobileNumber']; 
                // Loading message library.
                $this->load->library("send_sms");
                // Sending Otp to user.
                $this->send_sms->sendOTP($phone,$otp_number);
                // Redirect to smsVerification page
                redirect(site_url('smsVerification'));
        }else{  
                // If no session is present then redirect to homepage.
                redirect(site_url(''));   
        } 
}

}