<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payumoney extends CI_Controller {

	public function index($paymentAmtType, $orderID, $requestFrom)
	{
		// Merchant key here as provided by Payu
		$MERCHANT_KEY = "5Wc5S6";

		// Merchant Salt as provided by Payu
		$SALT = "bwKcgXDB";

		// End point - change to https://secure.payu.in for LIVE mode
		$PAYU_BASE_URL = "https://secure.payu.in";
		/*
		if($requestFrom == 'app')
		{
			$MERCHANT_KEY = "TfaPzGxW";
			$SALT = "KhiQXnMOot";
			$PAYU_BASE_URL = "https://test.payu.in";
		}
		*/
		
		// Generate random transaction id
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		
		$sql = "SELECT a.`total`,a.`orderId`,b.`firstName`, b.`emailId`, b.`mobileNumber` 
				FROM `ordertb` a 
				JOIN `customer` b
				ON a.`customerId` =b.`customerId`
				WHERE a.`orderId` = '$orderID';
				SELECT sum(`rentalTotalPrice`/`duration`/`qty`) as odr
				FROM `orderproduct` 
				WHERE `subOrderID` in (
					SELECT `subOrderID` 
					FROM `tbl_suborder` 
					WHERE `orderID` = '$orderID' 
					and `status` in (1, 2, 3, 8)
				);";

		$result = $this->bml_database->getResults($sql);
		if(!array_key_exists(0, $result) || !array_key_exists(0, $result[0]) || !array_key_exists('orderId', $result[0][0]))
		{
			die("Something went wrong.Please contact admin@bookmylens.com");
		}
		if(!array_key_exists(1, $result) || !array_key_exists(0, $result[1]) || !array_key_exists('odr', $result[1][0]))
		{
			die("Something went wrong.Please contact admin@bookmylens.com");
		}
		$odr = $result[1][0]['odr'];
		$result = $result[0][0];		
		$productInfoArr['paymentParts'][] = array('name'=>'Canon', 'description'=>'', 'value'=>'1000', 'isRequired'=>'true', 'settlementEvent'=>'EmailConfirmation');
		$posted['key'] = $MERCHANT_KEY;
        $posted['txnid'] = $txnid;
        $posted['amount'] = ($paymentAmtType == 'ODR') ? $odr : $result['total'];
        $posted['firstname'] = $result['firstName'];
        $posted['email'] = $result['emailId'];
        $posted['phone'] = $result['mobileNumber'];
        $posted['productinfo'] = '{}';
        $posted['surl'] = 'http://bookmylens.com/payumoney/success/'.$requestFrom;
        $posted['furl'] = 'http://bookmylens.com/payumoney/failure/'.$requestFrom;
        $posted['curl'] = 'http://bookmylens.com/payumoney/cancel/'.$requestFrom;
		$posted['service_provider'] = 'payu_paisa';
		  
		$hash = '';
		// Hash Sequence
		$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

		$hashVarsSeq = explode('|', $hashSequence);
		$hash_string = '';	
		foreach($hashVarsSeq as $hash_var) {
		  $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
		  $hash_string .= '|';
		}

		$hash_string .= $SALT;

		$posted['hash'] = strtolower(hash('sha512', $hash_string));

		$action = $PAYU_BASE_URL . '/_payment';
		
		$sql = "insert into `tbl_pum_txn`(`txnID`, `amount`, `custID`, `orderID`) 
				values('$txnid', '".$posted['amount']."', '".$this->user_session->getSessionVar('customerId')."', '$orderID')";
		$this->db->query($sql);
		
		echo '
		<body>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
			<script>
				$(function(){ $("#form").submit();});
			</script>
			<form id="form" action="'.$action.'" method="POST"/>';
					foreach($posted as $key => $value)
					{
						echo "<input type=\"hidden\" name=\"$key\" value=\"$value\">";
					}
		echo 	'
		</form>
		</body>';
		
	}
	
	public function cancel($requestFrom){}
	
	public function failure($requestFrom) {

		//print_r($_POST);
		// Merchant key here as provided by Payu
		$MERCHANT_KEY = "5Wc5S6";

		// Merchant Salt as provided by Payu
		$salt = "bwKcgXDB";
		/*
		if($requestFrom == 'app')
		{
			$MERCHANT_KEY = "TfaPzGxW";
			$salt = "KhiQXnMOot";
		}
		*/
		$productinfo=$_POST["productinfo"];
		$firstname=$_POST["firstname"];
		$status=$_POST["status"];
		$amount=isset($_POST["amount"]) ? $_POST["amount"] : 0;
		$pumError=isset($_POST["Error"]) ? $_POST["Error"] : '';
		$unmappedstatus=isset($_POST["unmappedstatus"]) ? $_POST["unmappedstatus"] : '';
		$payuMoneyId=isset($_POST["payuMoneyId"]) ? $_POST["payuMoneyId"] : '';
		$mihpayid=isset($_POST["mihpayid"]) ? $_POST["mihpayid"] : '';
		$txnid=$_POST["txnid"];
		print_r($_POST);
		
		$posted_hash=$_POST["hash"];
		$key=$_POST["key"];
		$email=$_POST["email"];
		
		If (isset($_POST["additionalCharges"])) {
			$additionalCharges=$_POST["additionalCharges"];
			$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		}
		else {	  
			$retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		}
		
		$hash = hash("sha512", $retHashSeq);

		$this->load->view('head');
		$data['category'] = $this->bml_read_json->readRentMenu();
	
		if ($hash != $posted_hash) {
		   echo "Invalid Transaction. Please try again";
		}
		else {
			$sql = "update tbl_pum_txn set
					`pumUniqueID` = '$mihpayid',
					`status` = '$status',
					`txnError` = '$pumError',
					`pumID` = '$payuMoneyId',
					`unmappedstatus` = '$unmappedstatus',
					`returnJSON` = json_encode($_POST)
					where `txnID` = '$txnid';
					SELECT a.`orderID`, b.deliveryAddress, b.pickupAddress 
					FROM `tbl_pum_txn` a, `ordertb` b
					WHERE a.`txnID` = '$txnid'
					and a.orderID = b.orderId;";
					
			$result = $this->bml_database->getResults($sql);
			
			//print_r($result);
			if(!array_key_exists(1, $result) || !array_key_exists(0, $result[1]) || !array_key_exists('orderID', $result[1][0]))
			{
				die("Something went wrong. Please contact admin@bookmylens.com");
			}
			$result = $result[1][0];
			
			$this->load->model('orderModel');
			$orderDetails = $this->orderModel->getOrderDetails($result['orderID']);
      
			$data = array_merge($data, $orderDetails[0][0]);

			$subOrderItems = [];
			foreach ($orderDetails[1] as $orderItem) {
				$subOrderItems[$orderItem['orderNumber']][] = $orderItem;
			}
			$data['subOrderItems'] = $subOrderItems;
			$addressRet = $this->orderModel->getClientAddressbyID($result['deliveryAddress']);
			$data['pickupAddr'] = $addressRet[0][0];
			$data['shippingCost'] = $data['pickupAddr']['shippingCost'];

			$addressRet = $this->orderModel->getClientAddressbyID($result['pickupAddress']);
			$data['returnAddr'] = $addressRet[0][0];
			$data['shippingCost'] += $data['returnAddr']['shippingCost'];
		
			$this->load->library("send_email");

			$eData['total'] = $data['total'];
			$eData['email'] = $this->user_session->getSessionVar('emailID');
			$eData['firstName'] = $this->user_session->getSessionVar('firstName');
			$eData['customerNumber'] = $this->user_session->getSessionVar('customerNumber');
			$eData['subOrderItems'] = $subOrderItems;
			$eData['txnFailed'] = $data['txnFailed'] = $txnid;
			$this->send_email->sendOrderSummaryEmail($eData);
			
					
			  $subject="PayUMoney Payment Failed | ".$eData['firstName']."(".$eData['customerNumber'].") | ".$txnid;

			  $this->email->set_newline("\r\n");
			  $email_body = "PayUMoney Payment Failed for Transaction ID is $txnid. Please contact customer as soon as possible";
			  $this->email->from("rentals@bookmylens.com", 'BookMyLens');
			  $list = array($to);
			  $this->email->to("rentals@bookmylens.com");
			  $this->email->subject($subject);
			  $this->email->message($email_body);
			  $this->email->send();
			
			//breadcrumb
			$breadcrumb = [];
			$breadcrumb[] = array('link' => site_url(), 'name'=>'Home');
			$breadcrumb[] = array('link' => '', 'name' => 'Checkout Success');
			$data['breadcrumb'] = $breadcrumb;
			
			$this->load->view('header',$data);
			$this->load->view('checkoutsuccess');
			$this->load->view('footer');
			
		}
	}
	
	public function success($requestFrom) {
		// Merchant key here as provided by Payu
		$MERCHANT_KEY = "5Wc5S6";

		// Merchant Salt as provided by Payu
		$salt = "bwKcgXDB";

		/*
		if($requestFrom == 'app')
		{
			$MERCHANT_KEY = "TfaPzGxW";
			$salt = "KhiQXnMOot";
		}
		*/
		
		$status=$_POST["status"];
		$productinfo=$_POST["productinfo"];
		$firstname=$_POST["firstname"];
		$amount=isset($_POST["amount"]) ? $_POST["amount"] : 0;
		$discount=isset($_POST["discount"]) ? $_POST["discount"] : 0;
		$pumError=isset($_POST["Error"]) ? $_POST["Error"] : '';
		$unmappedstatus=isset($_POST["unmappedstatus"]) ? $_POST["unmappedstatus"] : '';
		$payuMoneyId=isset($_POST["payuMoneyId"]) ? $_POST["payuMoneyId"] : '';
		$mihpayid=isset($_POST["mihpayid"]) ? $_POST["mihpayid"] : '';
		$mode=isset($_POST["mode"]) ? $_POST["mode"] : '';
		$pgType=isset($_POST["PG_TYPE"]) ? $_POST["PG_TYPE"] : '';
		$refNum=isset($_POST["bank_ref_num"]) ? $_POST["bank_ref_num"] : '';
		$txnid=$_POST["txnid"];

		$posted_hash=$_POST["hash"];
		$key=$_POST["key"];
		$email=$_POST["email"];
		
		If (isset($_POST["additionalCharges"])) {
			$additionalCharges=$_POST["additionalCharges"];
			$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		}
		else {	  
			$retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		}
		
		$hash = hash("sha512", $retHashSeq);
		  
		  
		$this->load->view('head');
		$data['category'] = $this->bml_read_json->readRentMenu();

		if ($hash != $posted_hash) {
		   echo "Invalid Transaction. Please try again";
		}
		else {
			$sql = "update tbl_pum_txn set
					`pumUniqueID` = '$mihpayid',
					`status` = '$status',
					`txnError` = '$pumError',
					`pumID` = '$payuMoneyId',
					`unmappedstatus` = '$unmappedstatus',
					`mode` = '$mode',
					`pumDiscount` = '$discount',
					`pgType` = '$pgType',
					`bankRefID` = '$refNum',
					`returnJSON` = '".json_encode($_POST)."'
					where `txnID` = '$txnid';
					SELECT a.`orderID`, b.deliveryAddress, b.pickupAddress 
					FROM `tbl_pum_txn` a, `ordertb` b
					WHERE a.`txnID` = '$txnid'
					and a.orderID = b.orderId;";
					
			$result = $this->bml_database->getResults($sql);
			
			if(!array_key_exists(1, $result) || !array_key_exists(0, $result[1]) || !array_key_exists('orderID', $result[1][0]))
			{
				die("Something went wrong.Please contact admin@bookmylens.com");
			}
			$result = $result[1][0];
			$sql = "SELECT `id` INTO @pumID FROM `tbl_pum_txn` WHERE `txnID` = '$txnid';
					insert into order_payment(`pumID`, `orderID`, `paymentType`, `amount`, `referenceNumber`, `bankName`, `branch`, `status`, `adminID`, `date`, `createdDate`)
					values(@pumID, '".$result['orderID']."', 2, '$amount', '$payuMoneyId', 'PayUMoney', '', 0, -1, now(), now());";
			$this->bml_database->getResults($sql);
			
			$this->load->model('orderModel');
			$orderDetails = $this->orderModel->getOrderDetails($result['orderID']);
      
			$data = array_merge($data, $orderDetails[0][0]);

			$subOrderItems = [];
			foreach ($orderDetails[1] as $orderItem) {
				$subOrderItems[$orderItem['orderNumber']][] = $orderItem;
			}
			$data['subOrderItems'] = $subOrderItems;
			$addressRet = $this->orderModel->getClientAddressbyID($result['deliveryAddress']);
			$data['pickupAddr'] = ($addressRet[0][0]) ?$addressRet[0][0]: array();
			$data['shippingCost'] = ($data['pickupAddr']['shippingCost']) ?$data['pickupAddr']['shippingCost'] : 0;

			$addressRet = $this->orderModel->getClientAddressbyID($result['pickupAddress']);
			$data['returnAddr'] = ($addressRet[0][0]) ?$addressRet[0][0] :array();
			$data['shippingCost'] += (($data['returnAddr']['shippingCost']) ?$data['returnAddr']['shippingCost'] : 0);
		
			$this->load->library("send_email");

			$eData['total'] = $data['total'];
			$eData['email'] = $this->user_session->getSessionVar('emailID');
			$eData['firstName'] = $this->user_session->getSessionVar('firstName');
			$eData['customerNumber'] = $this->user_session->getSessionVar('customerNumber');
			$eData['subOrderItems'] = $subOrderItems;
			$eData['pumAmt'] = $data['pumAmt'] = $amount;
			$this->send_email->sendOrderSummaryEmail($eData);
			
			//breadcrumb
			$breadcrumb = [];
			$breadcrumb[] = array('link' => site_url(), 'name'=>'Home');
			$breadcrumb[] = array('link' => '', 'name' => 'Checkout Success');
			$data['breadcrumb'] = $breadcrumb;
			if($requestFrom == 'app')
			{
				$this->load->view('checkoutsuccess',$data);
			}
			else
			{
				$this->load->view('header',$data);
				$this->load->view('checkoutsuccess');
				$this->load->view('footer');
			}
		}
	}
}
