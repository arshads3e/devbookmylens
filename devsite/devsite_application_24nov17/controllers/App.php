<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

  public $validToken = false;
  public $token = '';
  public $user = '';
  public $userId = -1;
  
  public function __construct()
  {
	// Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		exit(0);
	}
	parent::__construct();
	$this->load->model('appModel');
	$this->validateToken();
  }
  /*
  function __destruct(){
	$this->session->sess_destroy();
  }
  */
  
  protected function validateToken()
  {
	if(isset($_SERVER['HTTP_AUTHORIZATION']) && $_SERVER['HTTP_AUTHORIZATION'] != '')
	{
		$tokenTxt = $_SERVER['HTTP_AUTHORIZATION'];
		$tokenTxt = trim(preg_replace('/^OAuth/', '', $tokenTxt));
		$tokenTxt = base64_decode($tokenTxt);
		$tokenArr = explode(':', $tokenTxt);
		$this->token = $tokenArr[0];
		$this->user = $tokenArr[1];
		$arr_out = $this->appModel->validateToken($this->token, $this->user);
		$this->userId = $arr_out['login'];
		$this->status = $arr_out['status'];
		$this->validToken = ($this->userId != -1);
		if($this->validToken)
		{
			$this->setSessionVar('emailID',$this->user);
			$this->setSessionVar('loggedIn',TRUE);
			$this->setSessionVar('customerId', $this->userId);
		}
	}
	else
	{
		$this->validToken = false;
	}
  }
  
  private function setSessionVar($name,$value)
  {
    $this->session->set_userdata($name,$value);
  }
  public function index()
  {
	return json_encode(array());
  }
  
  public function register()
  {
	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
		$request = json_decode($postdata)->data;
		
		$fname = $request->firstName;
		$lname = $request->lastName;
		$emailId = $request->email;
		$pwd1 = $request->pass;
		$pwd2 = $request->cpass;
		$phone = $request->number;
		$gender = $request->gender;
		$companyPerson = $request->cp;

		$json = [];
		if($fname == "" || $emailId == "" || $pwd1 == "" || $pwd2 == "" || $phone == "" || $gender == "" || $companyPerson=="")
		{
		  $json = array("status"=>false,"errorcode" => "BLANK","error"=>"required fields must be filled");
		}
		else if($pwd1 != $pwd2)
		{
		  $json = array("status"=>false,"errorcode" => "MISMATCH","error"=>"password mismatch");
		}
		else
		{
		  $pwd1 = md5($pwd1);
		  $this->load->model('myaccountModel');
		  if($this->myaccountModel->checkEmailExists($emailId))
		  {
			$json = array("status"=>false,"errorcode" => "EXISTS","error"=>"Account already exists. Please Login.");
		  }
		  else {
			$result = $this->myaccountModel->registerUser($companyPerson,$fname,$lname,$gender, $emailId,$pwd1,$phone);
			$code = $result[0][0]['varReferralCode'];
			$customerID = $result[0][0]['customerID'];
			$customerNumber = $result[0][0]['customerNumber'];
			
			$this->load->library("send_email");
			$data['email'] = $emailId;
			$data['firstName'] = $fname;
			$data['code'] = $code;
			$data['customerID'] = $customerID;
			$data['customerNumber'] = $customerNumber;
			$this->send_email->sendWelcomeEmail($data);
			$json = array("status"=>true,"message"=>"Registration Successful, Please check your mail to confirm your email");
		  }
		}

		echo json_encode($json);
	}
	else {
		echo json_encode(array('status' => false, 'token' => '', 'error'=> 'No Data'));
	}
  }
  
  public function reset()
  { 
	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
		$request = json_decode($postdata);
		$emailID = $request->email;
		$pwd = randomPassword();
		$sql = "SELECT ifnull(`customerId`, -1) as custID, `customerNumber`, `firstName` FROM `customer` WHERE `emailId` = '$emailID';
				UPDATE customer set `loginPassword` = '$pwd' where `emailId` = '$emailID';";
		$res = $this->bml_database->getResults($sql);
		
		if(!array_key_exists(0,$res) || !array_key_exists(0,$res[0]))
		{
			echo json_encode(array("status"=>"false","error"=>"email does not exists"));
		}
		else
		{
			
			$this->load->library("send_email");
			$data['email'] = $emailID;
			$data['pass'] = $pwd;
			$data['firstName'] = $res[0][0]['firstName'];
			$data['customerID'] = $res[0][0]['custID'];
			$data['customerNumber'] = $res[0][0]['customerNumber'];
			
			$this->send_email->sendPassResetEmail($data);
			echo json_encode(array("status"=>"true","message"=>"Password Reset Successful"));
		}
	}
	else {
		echo json_encode(array('status' => false, 'token' => '', 'error'=> 'No Data'));
	}
  }
  
  public function checkLogin() {
	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
		$request = json_decode($postdata);
		$res = $this->appModel->checkLogin($request->username, $request->password);
		if($res)
		{
			echo json_encode(array('status' => true, 'token' => '', 'error'=> ''));
		}
		else
		{
			echo json_encode(array('status' => false, 'token' => '', 'error'=> 'Login/Pass'));
		}
	}
	else {
		echo json_encode(array('status' => false, 'token' => '', 'error'=> 'No Data'));
	}
  }
  
  public function login() {
	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
		$request = json_decode($postdata);
		echo $this->appModel->login($request->username, $request->password);
	}
	else {
		echo json_encode(array('status' => false, 'token' => '', 'error'=> 'No Data'));
	}
  }
  
  public function logout() {
	if($this->validToken)
	{
		$status = $this->appModel->logout($this->token, $this->user);
		echo json_encode(array('status' => $status, 'data' => '', 'error' => ''));
	}
	else
	{
		echo json_encode(array('status'=>false, 'error'=>'login'));
	}
  }
  
  public function getMainPageTiles() {
	$this->load->model('commonModel');
	$resultSet = $this->commonModel->getMainPageCategories();
	$data = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();
	echo json_encode(array('status' => true, 'data' => $data));

  }
  
  public function getProductsByCategoryId() {
	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
		$request = json_decode($postdata);
		$this->load->model('appModel');
		$data = $this->appModel->getProductsByCategoryId($request->categoryId, $request->categoryType);
		echo json_encode(array('status' => true, 'data' => $data));

	}
	else {
		echo json_encode(array('status' => false, 'token' => '', 'error'=> 'No Data'));
	}
  }
  
  public function getClientAddress() {
	if($this->validToken)
	{
		$this->load->model('orderModel');
		$result = $this->orderModel->getOrderCount();
		$orderCount = array_key_exists(0, $result)? (array_key_exists(0, $result[0])? (array_key_exists('count', $result[0][0])? $result[0][0]['count']: 0 ) : 0 ): 0;
		$data = array();
		if($orderCount > 0){
			$result = $this->orderModel->getClientAddress($this->userId);
			$data = array_key_exists(0, $result)? $result[0]: array();
		}
		echo json_encode(array('status' => true, 'data' => $data));
	}
	else
	{
		echo json_encode(array('status'=>false, 'error'=>'login'));
	}
  }
  public function verifyCoupon() {
	if($this->validToken)
	{
		$postdata = file_get_contents("php://input");
		if (isset($postdata)) {
			$request = json_decode($postdata);
			
			$this->load->model('orderModel');
			$result = $this->orderModel->getDiscountDetails($request->coupon);
			$discountCount = array_key_exists(1, $result)? (array_key_exists(0, $result[1])? (array_key_exists('count', $result[1][0])? $result[1][0]['count']: 0 ) : 0 ): 0;
			$discountDetails = array_key_exists(0, $result)? (array_key_exists(0, $result[0])? $result[0][0] : array() ): array();
			if(empty($discountDetails))
			{
				echo json_encode(array('status' => false, 'error' =>'Invalid Token', 'data' => null));
			}
			else
			{
				
				if($discountDetails['customerIds'] != "0")
				{
				  if(!  in_array(
						  $this->userId,
						  explode(",",$discountDetails['customerIds'])
						)
					) {
					echo json_encode(array("status" => false, "error" => "Not Applicable"));
				  }
				  else if($discountDetails['discountCount'] > 0
					  && $discountCount >= $discountDetails['discountCount']){
					echo json_encode(array("status" => false, "error" => "Not Applicable"));
				  }
				  else {
					$this->cart->applyDiscount($discountDetails['discountId'], $discountDetails['promocode'], $discountDetails['discount'], $discountDetails['discountType']);
					echo json_encode(array("status" => true, "data" => $discountDetails));
				  }
				}
				else {
				  $this->cart->applyDiscount($discountDetails['discountId'], $discountDetails['promocode'], $discountDetails['discount'], $discountDetails['discountType']);
				  echo json_encode(array("status" => true, "data" => $discountDetails));
				}
			}
		}
		else {
			echo json_encode(array('status' => false, 'token' => '', 'error'=> 'No Data'));
		}
	}
	else
	{
		echo json_encode(array('status'=>false, 'error'=>'login'));
	}
  }
  
  public function getAllProductList() {
	$sql = "SELECT b.itemBrandName, c.category_Name as category_Name, a.`itemId`, a.`itemName`, d.price
		FROM `itemmaster` a
		JOIN tbl_item_brand b
		on a.`itemBrand` = b.itemBrandID
		JOIN category_tb c
		ON a.`itemCategory` = c.categoryID
		JOIN pricemaster d
		ON a.itemId = d.itemId
		JOIN (select itemId, min(days) as days from pricemaster group by itemId) t1
		ON a.itemId = t1.itemId
		WHERE d.days = t1.days
		and a.seasonID = d.seasonId
		and a.`itemStatus` = 1";
	$result = $this->bml_database->getResults($sql);
	$data = (array_key_exists(0, $result) ? $result[0]: array());
	echo json_encode(array('status'=>true, 'data'=> $data));
  }
  public function getProductRentDetailsById() { 
	$this->load->helper('file');
	$data = $this->validToken.' : '.$this->status;
    
	if($this->validToken)
	{
		if($this->status == 1)
		{
			$postdata = file_get_contents("php://input");
			if (isset($postdata)) {
				$request = json_decode($postdata);
				$this->load->model('appModel');

				$data = array();
				$data['productDetails'] = $this->appModel->getProductDetailsById($request->productId);
				
				$this->load->model('orderModel');
				$availableCountRes = $this->orderModel->getAvailableQtyByItemId($data['productDetails']['itemId']);
				$availableQty = (array_key_exists(0, $availableCountRes)) ? $availableCountRes[0]: array();

				$totalQty = (array_key_exists(1, $availableCountRes)) ? $availableCountRes[1][0]['totalQty']: array();
				
				$unAvailableQtyArr = array();
				$availableQtyArr = array();
				foreach ($availableQty as $availableQtyRow) {
					if($availableQtyRow['availableQty'] <= 0){
						$unAvailableQtyArr[] = date_format(date_create($availableQtyRow['dates']), "Y-m-d");
					}
					$availableQtyArr[date_format(date_create($availableQtyRow['dates']), "Y-m-d")] = $availableQtyRow['availableQty'];
				}
				$data['unAvailableDates'] = $unAvailableQtyArr;
				$data['availableQty'] = $availableQtyArr;

				$holidayListRes = $this->orderModel->getHolidayList();
				$holidayList = (array_key_exists(0, $holidayListRes)) ? $holidayListRes[0]: array();
				$holidayListArr = array();
				foreach ($holidayList as $holidayListRow) {
					$holidayListArr[] = $holidayListRow['holidayDate'];
				}
				$data['holidayList'] = $holidayListArr;

				echo json_encode(array('status'=>true, 'data'=> $data));
			}
		}	
		else {
			echo json_encode(array('status' => false, 'token' => '', 'error'=> 'NoAccess'));
		}
		
	}
	else
	{
		echo json_encode(array('status'=>false, 'error'=>'login'));
	}
  }
  
  private function loadUserDetailsToSession() {
	
    $this->load->model('myaccountModel');
    $details=$this->myaccountModel->getUserSessionDetails();
    $this->setSessionVar('firstName',$details['firstName']);
    $this->setSessionVar('lastName',$details['lastName']);
    $this->setSessionVar('customerId',$details['customerId']);
    $this->setSessionVar('customerNumber',$details['customerNumber']);
  }
  
  public function myaccount(){
	if($this->validToken)
	{
		$this->loadUserDetailsToSession();
		$this->load->model('myaccountModel');
		$data['terms']=$this->myaccountModel->checkTermsFlag();

		$customerDetailsResult=$this->myaccountModel->getCustomerInfo();
		$customerDetails = (array_key_exists(0, $customerDetailsResult)) ? $customerDetailsResult[0]: array();//get first resultset
		$customerDetails = (array_key_exists(0, $customerDetails)) ? $customerDetails[0]: array();//get first row
		$sql="SELECT * FROM `company_details` WHERE `companyID`='".$customerDetails['companyID']."'";
		$companyAddressResult=$this->bml_database->getResults($sql);
		$companyAddress = (array_key_exists(0, $companyAddressResult)) ? $companyAddressResult[0]: array();//get first resultset
		$companyAddress = (array_key_exists(0, $companyAddress)) ? $companyAddress[0]: array();
		/*print_r($companyAddress);*/

		$data['companyAddress']=$companyAddress;
		$data['customerDetails']=$customerDetails;


		$customerAddressResult = (array_key_exists(1, $customerDetailsResult)) ? $customerDetailsResult[1]: array();//get first resultset
		$customerAddress=[];
		foreach ($customerAddressResult as  $customerAddressRow) {
		 $customerAddress[$customerAddressRow['addressType']][]=$customerAddressRow;
		}
		$homeAddress='';
		if(!empty($customerAddress['HOME'][0]))
		{
		  $homeAddress=$customerAddress['HOME'][0];
		}
		$officeAddress='';
		if(!empty($customerAddress['OFFICE'][0]))
		{
		  $officeAddress=$customerAddress['OFFICE'][0];
		}
		$result = $this->myaccountModel->getOrderListByStatus();
		$result = (array_key_exists(0, $result))? $result[0]: array();

		$this->load->model('orderModel');
		$paymentMaster = $this->orderModel->getPaymentMaster();
		$paymentMaster = (array_key_exists(0, $paymentMaster))? $paymentMaster[0]: array();
		$paymentOptions = '';
		foreach ($paymentMaster as $paymentMasterRow) {
		  $paymentOptions .= "<option value='".$paymentMasterRow['typeID']."'>".$paymentMasterRow['typeDisplayName']."</option>";
		}
		$data['paymentOptions'] = $paymentOptions;

		$paymentRestult = $this->orderModel->getPaymentDetails();
		$paymentRestult1 = (array_key_exists(0, $paymentRestult))? $paymentRestult[0]: array();
		$paymentRestult2 = (array_key_exists(1, $paymentRestult))? $paymentRestult[1]: array();

		$orderNumbersOption = '';
		$paymentDetails = [];
		foreach ($paymentRestult2 as $paymentRow) {
		  $paymentRow['pending'] = $paymentRow['total'] - $paymentRow['paid'];
		  $paymentDetails[$paymentRow['orderID']] = $paymentRow;
		  if($paymentRow['pending'] > 0)
		  {
			$orderNumbersOption .= "<option value='".$paymentRow['orderID']."'>".$paymentRow['orderNumbers']."</option>";
		  }
		}
		foreach ($paymentRestult1 as $paymentRow) {
		  $paymentDetails[$paymentRow['orderID']]['details'][] = $paymentRow;
		}
		//print_r($paymentDetails);

		$data['orderNumbersOption'] = $orderNumbersOption;
		$data['paymentDetails'] = $paymentDetails;

		for($i=0; $i<count($result); $i++) {
		  $suborderId= $result[$i]['subOrderID'];
		  $sql="SELECT  a.`startDate`,a.`endDate`,a.`collectDate`,a.`returnDate`,a.`rentalTotalPrice`,b.`itemName`,c.`orderStatusDisplayName`
				FROM `orderproduct` a
				JOIN `itemmaster` b
				on a.`itemId` = b.`itemId`
				JOIN `tbl_order_status` c
				on a.`status` = c.`orderStatusID`
				WHERE `subOrderID`='".$suborderId."'";
		  $itemIdList=$this->bml_database->getResults($sql);
		  $itemIdList = (array_key_exists(0, $itemIdList))? $itemIdList[0]: array();
		  $result[$i]['items']= $itemIdList;

		}

		$data['orderDetails'] = $result;

		$data['customerAddress']=$customerAddress;
		$data['homeAddress']=$homeAddress;
		$data['officeAddress']=$officeAddress;

		$sql = "SELECT sum(`points`) as 'totalpoints', `pointsType` FROM `points_vallet` WHERE `customerId` = '".$this->user_session->getSessionVar('customerId')."' group by `pointsType`;
				SELECT a.*,b.promocode from `points_vallet` a 
				LEFT JOIN `discount` b 
				on a.discountId = b.discountID
				WHERE `customerId` = '".$this->user_session->getSessionVar('customerId')."';";
		$points_vallet_res = $this->bml_database->getResults($sql);
		$points_vallet_summary_arr = (array_key_exists(0, $points_vallet_res)) ? $points_vallet_res[0]: array();//get first resultset
		$points_vallet_summary = array();
		foreach($points_vallet_summary_arr as $row)
		{
			$points_vallet_summary[$row['pointsType']] = $row['totalpoints'];
		}
		$data['points_vallet_summary'] = $points_vallet_summary;
		
		$points_vallet_details_arr = (array_key_exists(1, $points_vallet_res)) ? $points_vallet_res[1]: array();//get first resultset
		
		$points_vallet_details = array();
		foreach($points_vallet_details_arr as $row)
		{
			$points_vallet_details[$row['pointsType']][] = $row;
		}
		$data['points_vallet_details'] = $points_vallet_details;
		
		$sql = "set @custID = '".$this->user_session->getSessionVar('customerId')."'; 
					SELECT `promocode` FROM `discount` WHERE `discountId` = (
						SELECT `discountID` FROM `points_vallet` WHERE `discountID` not in (
							SELECT `discountID` FROM `ordertb` WHERE `customerId` = @custID  and discountID > 0) 
						and `customerId` = @custID and discountID > 0) and discountType = 'FIXED';";
		$promocode_res = $this->bml_database->getResults($sql);
		$promocode_res = (array_key_exists(1, $promocode_res)) ? $promocode_res[1]: array();
		$promocode_res = (array_key_exists(0, $promocode_res)) ? $promocode_res[0]: array();
		$data['promocode'] = (array_key_exists('promocode', $promocode_res)? $promocode_res['promocode']: null);
		
		echo json_encode(array('status' => true, 'token' => '', 'data' => $data));
	}
	else {
		echo json_encode(array('status' => false, 'token' => '', 'error'=> 'No Data'));
	}
  }
  
  public function placeOrder(){

	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
		$request = (array)json_decode($postdata);
		//print_r ($request);
		$error = array();
		foreach($request['_items'] as $item)
		{
			$item = (array)$item;
			$item['startDate'] = date_create($item['startDate']);
			$item['startDate']->add(new DateInterval('P1D'));
			$item['startDate'] = date_format($item['startDate'], 'd/M/Y');
			
			$item['endDate'] = date_create($item['endDate']);
			$item['endDate']->add(new DateInterval('P1D'));
			$item['endDate'] = date_format($item['endDate'], 'd/M/Y');
			
			$ret = $this->insertProduct($item['id'], $item['name'], $item['qty'], $item['price'], $item['days'], $item['startDate'], $item['endDate']);
			
			if(!$ret['status'])
			{
				$error[] = array('productName' => $item['name'], 'message' => $ret['error']);
			}
		}
		
		if(count($this->cart->contents()) > 0 && count($error) == 0)
		{
			if($request['_discountId'] >= 0)
				$this->cart->applyDiscount($request['_discountId'], $request['_discountCode'], $request['_discount'], (($request['_discountType'] == 1)? "PERCENT": "FIXED"));
			
			$this->cart->setPickupAddrID($request['_pickupAddr']->addressID);
			$this->cart->setReturnAddrID($request['_returnAddr']->addressID);

			$result = $this->cart->placeOrder();
			$this->cart->destroy();
			$data = array();
			
			if($result['status'])
			{
				$this->load->model('orderModel');
				$orderDetails = $this->orderModel->getOrderDetails($result['orderID']);
				
				$subOrderItems = [];
				foreach ($orderDetails[1] as $orderItem) {
					$subOrderItems[$orderItem['orderNumber']][] = $orderItem;
				}
				$data = $orderDetails[0][0];
				$data['items'] = $subOrderItems;
				$data['subTotal'] = $request['_total'];
				$data['orderID'] = $result['orderID'];
				$this->loadUserDetailsToSession();
				
				$this->load->library("send_email");
				$eData['total'] = $orderDetails[0][0]['total'];
				$eData['email'] = $this->user_session->getSessionVar('emailID');
				$eData['firstName'] = $this->user_session->getSessionVar('firstName');
				$eData['customerNumber'] = $this->user_session->getSessionVar('customerNumber');
				$eData['subOrderItems'] = $subOrderItems;
				$this->send_email->sendOrderSummaryEmail($eData);
				echo json_encode(array('status' => true, 'token' => '', 'data' => $data));
			}
			else
			{
				echo json_encode(array('status' => false, 'token' => '', 'error'=> 'Error occurred during Order Placement'));
			}
		}
		else
		{
			if(count($error) == 0)
				$error[] = array('productName' => '', 'message' => 'No Items');
			echo json_encode(array('status' => false, 'token' => '', 'error'=> $error));
		}
	}
	else {
		echo json_encode(array('status' => false, 'token' => '', 'error'=> 'No Data'));
	}
  }
  
  public function getProductDetailsById() { 
	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
		$request = json_decode($postdata);
		$this->load->model('appModel');
		echo json_encode(array('status'=>true, 'data'=> $this->appModel->getProductDetailsById($request->productId)));
	}
	else {
		echo json_encode(array('status' => false, 'token' => '', 'error'=> 'No Data'));
	}
  }
  
  public function getCategories() { 
	echo json_encode(array('status'=>true, 'data'=> $this->appModel->getCategories()));
  }
  
  private function insertProduct($productID, $productName, $qty, $price, $days, $date1, $date2)
  {
	$collectDate = $date1;
	$returnDate = $date2;

	if($date2 == '')
		$returnDate = $date2 = $date1;
	
	$sd = date_create_from_format('d/M/Y',$date1);
	$ed = date_create_from_format('d/M/Y',$date2);
	
	$collectDate = date_create_from_format('d/M/Y',$collectDate);
	$returnDate = date_create_from_format('d/M/Y',$returnDate);
	$collectDate->sub(new DateInterval('P1D'));
	$returnDate->add(new DateInterval('P1D'));

	$sql = "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$collectDate->format('Y-m-d')."'  or `holidayDate` = '".$sd->format('Y-m-d')."'";
	$result = $this->bml_database->getResults($sql);
	$data['cannotCollect'] = $cannotCollect = (isset($result[0][0]['numRow']) && $result[0][0]['numRow'] == 2)?true:false;

	$sql = "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$returnDate->format('Y-m-d')."'  or `holidayDate` = '".$ed->format('Y-m-d')."'";
	$result = $this->bml_database->getResults($sql);
	$data['cannotReturn'] = $cannotReturn = (isset($result[0][0]['numRow']) && $result[0][0]['numRow'] == 2)?true:false;

	if($cannotReturn || $cannotCollect) {
		return array('status' => false, 'error' => 'we cannot deliver as we are on holiday on these days');
	}
	else {
	  
	  $waitingList = !$this->checkAvailProduct($productID, $sd, $ed);

	  $cartTtem = array(
		'id'      => $productID,
		'qty'     => $qty,
		'price'   => $price,
		'name'    => $productName,
		'options' => array('startDate' => date_format($sd, "d/M/Y"), 'endDate' => date_format($ed, "d/M/Y"), "waitingList" => $waitingList)
	  );
		
	  $this->cart->insert($cartTtem);
	  return array('status' => true, 'error' => '');
	}
	return array('status' => false, 'error' => '');
  }
  
  private function checkAvailProduct($productID, $date1, $date2)
  {
	$date1 = $date1->setTime(0,0);
	$date2 = $date2->setTime(0,0);
	
    $this->load->model('orderModel');
    $availableCountRes = $this->orderModel->getAvailableQtyByItemId($productID);
    //print_r($availableCountRes);
    $availableQty = (array_key_exists(0, $availableCountRes)) ? $availableCountRes[0]: array();
    $available = true;
    $cartDetails = $this->cart->get_item_by_id($productID);
    foreach ($availableQty as $availableQtyRow) {
        $date = date_create($availableQtyRow['dates']);
        $qty = $availableQtyRow['availableQty'];
        //echo $availableQtyRow['dates']."\n";
		if(($date1 <= $date)
              && ($date2 >= $date))
        {
          foreach ($cartDetails as $cartRow) {
            $itemOptions = $cartRow['options'];
            if(date_create_from_format('d/M/Y', $itemOptions['startDate']) <= $date && date_create_from_format('d/M/Y', $itemOptions['endDate']) >= $date)
            {
              $qty -= $cartRow['qty'];
              if($qty <= 0)
              {
                break;
              }
            }
          }

          if($qty <= 0)
          {
			$available = false;break;
          }
        }
    }
    return $available;
  }
}