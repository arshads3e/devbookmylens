<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

  public function index(){
    echo "this is cart file";
  }

  public function view()
  {
    if(!$this->user_session->isLoggedIn()) {
      redirect(site_url());die();
    } else if(!$this->user_session->getSessionVar('is_approved')){
      redirect(site_url('myaccount'));die();
    }
    $this->load->view('head');
    $data['category'] = $this->bml_read_json->readRentMenu();

    //breadcrumb
    $breadcrumb = [];
    $breadcrumb[] = array('link' => site_url(), 'name'=>'Home');
    $breadcrumb[] = array('link' => '', 'name'=>'cart');
    $data['breadcrumb'] = $breadcrumb;

    if($this->input->post('productName'))
    {
      $orderID = $this->input->post('orderID');
      if($orderID != '')
      {
        $this->cart->remove($orderID);
      }
      $date1 = $this->input->post('date1');
      $collectDate = $date1;

      $date2 = $this->input->post('date2');
      $returnDate = $date2;

      if($date2 == '')
        $returnDate = $date2 = $date1;
      $data['date1'] = $date1 = date_create_from_format('d/M/Y', $date1);
      $data['date2'] = $date2 = date_create_from_format('d/M/Y', $date2);
      $collectDate = date_create_from_format('d/M/Y', $collectDate);
      $returnDate = date_create_from_format('d/M/Y', $returnDate);
	  $collectDate->sub(new DateInterval('P1D'));
      $returnDate->add(new DateInterval('P1D'));
	  $data['collectDate'] = $collectDate->format('d/M/Y');
	  $data['returnDate'] = $returnDate->format('d/M/Y');
	  
	  $sql = "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$collectDate->format('Y-m-d')."'  or `holidayDate` = '".$date1->format('Y-m-d')."'";
      $result = $this->bml_database->getResults($sql);
	  $data['cannotCollect'] = $cannotCollect = (isset($result[0][0]['numRow']) && $result[0][0]['numRow'] == 2)?true:false;
	  
	  $sql = "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$returnDate->format('Y-m-d')."'  or `holidayDate` = '".$date2->format('Y-m-d')."'";
      $result = $this->bml_database->getResults($sql);
	  $data['cannotReturn'] = $cannotReturn = (isset($result[0][0]['numRow']) && $result[0][0]['numRow'] == 2)?true:false;
	  
	  $productName = $this->input->post('productName');
	  
	  if($cannotReturn || $cannotCollect) {
		
	  }
	  else {
		  $days = $this->input->post('days');
		  $price = $this->input->post('price');
		  $productName = $this->input->post('productName');
		  $productID = $this->input->post('productID');

		  $waitingList = !$this->checkAvailProduct($productID, $date1, $date2);

		  $cartTtem = array(
			'id'      => $productID,
			'qty'     => 1,
			'price'   => $price,
			'name'    => $productName,
			'options' => array('startDate' => date_format($date1, "d/M/Y"), 'endDate' => date_format($date2, "d/M/Y"), "waitingList" => $waitingList)
		  );
		  $this->cart->insert($cartTtem);
	  }
      $data['newProduct'] = $productName;
    }

    $this->load->view('header',$data);
    $this->load->view('cart');
    $this->load->view('footer');

  }

  public function update()
  {
    print_r($this->input->post);
  }

  function clear() {

    $this->cart->destroy();
    redirect('welcome');
  }
  function remove($rowid) {

    $this->cart->remove($rowid);
    redirect('cart/view');
  }
  function incQty()
  {
    $rowid = $this->input->post('rowid');
    $qty = $this->input->post('qty');
    $waiting = $this->input->post('waiting');
    $item = $this->cart->get_item($rowid);
    $productID = $item['id'];
    $json = [];

    //check if required postdata present.
    if($rowid == "" && $qty == "" )
    {
      $json = array("status"=>"failure","errorcode" => "BLANK","message"=>"ERROR");
    }
    //check if required qty present.
    else if($item['options']['waitingList'] != true && ! $this->checkAvailProduct($productID, date_create_from_format('d/M/Y', $item['options']['startDate']), date_create_from_format('d/M/Y', $item['options']['endDate'])))
    {
      //if no required qty and waiting is true, add product to waiting list
      if($waiting == 1)
      {
        $cartTtem = array(
          'id'      => $item['id'],
          'qty'     => 1,
          'price'   => $item['price'],
          'name'    => $item['name'],
          'options' => array('startDate' => $item['options']['startDate'], 'endDate' => $item['options']['endDate'], "waitingList" => true)
        );
        $this->cart->insert($cartTtem);

      }
      else {
        $json = array("status"=>"failure","errorcode" => "QTY","message"=>"qty not present");
      }
    }
    else {
       //if yes, increment count in cart
      $qty+=1;
      $data = array(
               'rowid' => $rowid,
               'qty'   => $qty,
               'waitingList' => false
            );
      $this->cart->update($data);
      $json = array("status"=>"success","qty"=>$qty, "subtotal"=>$this->cart->format_number($this->cart->total()), "discount"=>0, "total"=>$this->cart->format_number($this->cart->total()));
    }
    echo json_encode($json);

  }

  private function checkAvailProduct($productID, $date1, $date2)
  {
	$date1 = $date1->setTime(0,0);
	$date2 = $date2->setTime(0,0);
	
    $this->load->model('orderModel');
    $availableCountRes = $this->orderModel->getAvailableQtyByItemId($productID);
    //print_r($availableCountRes);
    $availableQty = (array_key_exists(0, $availableCountRes)) ? $availableCountRes[0]: array();
    $available = true;
    $cartDetails = $this->cart->get_item_by_id($productID);
    foreach ($availableQty as $availableQtyRow) {
        $date = date_create($availableQtyRow['dates']);
        $qty = $availableQtyRow['availableQty'];
        if(($date1 <= $date)
              && ($date2 >= $date))
        {
          foreach ($cartDetails as $cartRow) {
            $itemOptions = $cartRow['options'];
            if(date_create_from_format('d/M/Y', $itemOptions['startDate']) <= $date && date_create_from_format('d/M/Y', $itemOptions['endDate']) >= $date)
            {
              $qty -= $cartRow['qty'];
              if($qty <= 0)
              {
                break;
              }
            }
          }

          if($qty <= 0)
          {
            $available = false;break;
          }
        }
    }
    return $available;
  }

  function decQty()
  {

    $rowid = $this->input->post('rowid');
    $qty = $this->input->post('qty');
    $waiting = $this->input->post('waiting');
    $item = $this->cart->get_item($rowid);
    $productID = $item['id'];

    $json = [];

    //check if postdata present
    if($rowid == "" && $qty == "" )
    {
      $json = array("status"=>"failure","errorcode" => "BLANK","message"=>"ERROR");
    }
    //check if required qty present.
    else if($item['options']['waitingList'] != true && ! $this->checkAvailProduct($productID, date_create_from_format('d/M/Y', $item['options']['startDate']), date_create_from_format('d/M/Y', $item['options']['endDate'])))
    {
      //if no required qty and waiting is true, add product to waiting list
      if($waiting == 1)
      {
        $cartTtem = array(
          'id'      => $item['id'],
          'qty'     => 1,
          'price'   => $item['price'],
          'name'    => $item['name'],
          'options' => array('startDate' => $item['options']['startDate'], 'endDate' => $item['options']['endDate'], "waitingList" => true)
        );
        $this->cart->insert($cartTtem);
      }
      else {
        $json = array("status"=>"failure","errorcode" => "QTY","message"=>"qty not present");
      }
    }
    else {
       //if yes, decrment count in cart
      $qty-=1;
      $data = array(
               'rowid' => $rowid,
               'qty'   => $qty,
               'waitingList' => false
            );
      $this->cart->update($data);
      $json = array("status"=>"success","qty"=>$qty, "subtotal"=>$this->cart->format_number($this->cart->total()), "discount"=>0, "total"=>$this->cart->format_number($this->cart->total()));
    }
    echo json_encode($json);
  }

  public function checkout()
  {
    if(!$this->user_session->isLoggedIn()) {
      redirect(site_url());die();
    } else if(!$this->user_session->getSessionVar('is_approved')){
      redirect(site_url('myaccount'));die();
    }

    if(count($this->cart->contents()) <= 0){
      redirect(site_url());die();
    }
    $this->load->view('head');
    $data['category'] =  $this->bml_read_json->readRentMenu();

    $locationKey=$this->user_session->getSessionVar('locationKey');
   

    //breadcrumb
    $breadcrumb = [];
    $breadcrumb[] = array('link' => site_url(), 'name'=>'Home');
    $breadcrumb[] = array('link' => site_url('cart/view'), 'name'=>'cart');
    $breadcrumb[] = array('link' => '', 'name' => 'checkout');
    $data['breadcrumb'] = $breadcrumb;

    $this->load->model('orderModel');

    $result = $this->orderModel->getOrderCount();
    $orderCount = array_key_exists(0, $result)? (array_key_exists(0, $result[0])? (array_key_exists('count', $result[0][0])? $result[0][0]['count']: 0 ) : 0 ): 0;
    if($locationKey=="BNGLR"){
    $addressOption = "<option value='-1' data-price = '0'>Select</option>
                        <option value='1' title='' data-addr='' data-price = '0'>Store Pickup</option>";
    } else {
      $addressOption = "<option value='-1' data-price = '0'>Select</option>
      <option value='2' title='' data-addr='' data-price = '0'>Store Pickup</option>";
    }
      if($orderCount >0){
      $result = $this->orderModel->getClientAddress($locationKey);
    
	  $address = array_key_exists(0, $result)? $result[0]: array();
      foreach ($address as $addressRow) {
        if($addressRow['status'] == 0) continue;
		$addressText = $addressRow['addressLine1'].", ".$addressRow['addressLine2'].", ".$addressRow['addressCity'].", ".$addressRow['addressState'].", ".$addressRow['addressPin'];
        $addressOption .= "<option value='".$addressRow['addressID']."'  title='$addressText' data-addr='$addressText' data-price='".$addressRow['shippingCost']."'>".$addressRow['addressType']."</option>";
      }
    }

    $data['addressOption'] = $addressOption;
  //  var_dump($addressOption);die();
    
    $this->load->view('header',$data);
    $this->load->view('checkout');
    $this->load->view('footer');
  }

  public function checkoutsuccess()
  {
    
    if(!$this->user_session->isLoggedIn()) {
      redirect(site_url());die();
    } else if(!$this->user_session->getSessionVar('is_approved')){
      redirect(site_url('myaccount'));die();
    }
    if(count($this->cart->contents()) <= 0){
      redirect(site_url());die();
    }
    $pickup = $this->input->post('pickupAddr');
    $return = $this->input->post('returnAddr');
	//echo $pickup." : ".$return;
    if(!$pickup || !$return)
    {
      redirect(site_url());exit();
    }
	
    $this->load->view('head');
    $data['category'] = $this->bml_read_json->readRentMenu();


    //breadcrumb
    $breadcrumb = [];
    $breadcrumb[] = array('link' => site_url(), 'name'=>'Home');
    $breadcrumb[] = array('link' => '', 'name' => 'Checkout Success');
    $data['breadcrumb'] = $breadcrumb;


    $this->cart->setPickupAddrID($pickup);
    $this->cart->setReturnAddrID($return);
	
	$result = $this->cart->placeOrder();
    $this->cart->destroy();

    if($result['status'])
    {
		//$isPUM = $this->input->post('paymentType') == "PUM" && ($this->user_session->getSessionVar('emailID') == "agnesh2102@gmail.com" || $this->user_session->getSessionVar('emailID') == "rajiv.ranjan@payu.in");
		$isPUM = $this->input->post('paymentType') == "PUM";
		
		if($isPUM)
		{
			$eData['pumAmt'] = $data['pumAmt'] = 0;
		}
		
		$this->load->model('orderModel');
		$orderDetails = $this->orderModel->getOrderDetails($result['orderID']);
      
		$data = array_merge($data, $orderDetails[0][0]);

    $subOrderItems = [];
  
		foreach ($orderDetails[1] as $orderItem) {
			$subOrderItems[$orderItem['orderNumber']][] = $orderItem;
		}
		$data['subOrderItems'] = $subOrderItems;
		$addressRet = $this->orderModel->getClientAddressbyID($pickup);
		$data['pickupAddr'] = $addressRet[0][0];
		$data['shippingCost'] = $data['pickupAddr']['shippingCost'];

		$addressRet = $this->orderModel->getClientAddressbyID($return);
		$data['returnAddr'] = $addressRet[0][0];
		$data['shippingCost'] += $data['returnAddr']['shippingCost'];

		$sql = "SELECT sum(`points`) as 'totalpoints', `pointsType` FROM `points_vallet` WHERE `customerId` = '".$this->user_session->getSessionVar('customerId')."' group by `pointsType`;";
		$points_vallet_res = $this->bml_database->getResults($sql);
		$points_vallet_summary_arr = (array_key_exists(0, $points_vallet_res)) ? $points_vallet_res[0]: array();//get first resultset
		$points_vallet_summary = array();
		foreach($points_vallet_summary_arr as $row)
		{
			$points_vallet_summary[$row['pointsType']] = $row['totalpoints'];
		}
		$eDdata['points_vallet_summary'] = $points_vallet_summary;
		
		//print_r($data);
		$this->load->library("send_email");
        
		$eData['total'] = $data['total'];
		$eData['shippingCost'] = $data['shippingCost'];
		$eData['email'] = $this->user_session->getSessionVar('emailID');
			$eData['firstName'] = $this->user_session->getSessionVar('firstName');
		$eData['customerNumber'] = $this->user_session->getSessionVar('customerNumber');
		$eData['subOrderItems'] = $subOrderItems;
		$this->send_email->sendOrderSummaryEmail($eData);
		
		if($isPUM)
		{
			redirect(site_url("/Payumoney/index/".$this->input->post('paymentAmountType')."/".$result['orderID']."/web"));exit();
		} 
    }
    else {
		echo $result['message'];
    }
    $this->load->view('header',$data);
    $this->load->view('checkoutsuccess');
    $this->load->view('footer');
  }

  public function applyDiscount()
  {
    $discountCode = trim($this->input->post('coupon'));
    $discountAction = trim($this->input->post('discountAction'));
	
    if($discountAction == "clear")
    {
      $this->cart->clearDiscount();
      echo json_encode(array("status" => true, "message" => "clear"));
      return;
    }
	
    if(!$discountCode){
      echo json_encode(array("status" => false, "message" => "invalid code"));
    } else{
      $this->load->model('orderModel');
      $result = $this->orderModel->getDiscountDetails($discountCode);
      $discountCount = array_key_exists(1, $result)? (array_key_exists(0, $result[1])? (array_key_exists('count', $result[1][0])? $result[1][0]['count']: 0 ) : 0 ): 0;
      $discountDetails = array_key_exists(0, $result)? (array_key_exists(0, $result[0])? $result[0][0] : array() ): array();
	  if(empty($discountDetails))
      {
        echo json_encode(array("status" => false, "message" => "invalid code"));
      }
      else{
        if($discountDetails['customerIds'] != "0")
        {
          if(!  in_array(
                  $this->user_session->getSessionVar('customerId'),
                  explode(",",$discountDetails['customerIds'])
                )
            ) {
            echo json_encode(array("status" => false, "message" => "Not Applicable"));
          }
          else if($this->cart->getDiscount() > $discountDetails['discount'])
          {
            echo json_encode(array("status" => false, "message" => "Not Applicable"));
          }
          else if($discountDetails['discountCount'] > 0
              && $discountCount >= $discountDetails['discountCount']){
            echo json_encode(array("status" => false, "message" => "Not Applicable"));
          }
		  else {
			$this->cart->applyDiscount($discountDetails['discountId'], $discountDetails['promocode'], $discountDetails['discount'], $discountDetails['discountType']);
			echo json_encode(array("status" => true, "message" => "apply"));
		  }
        }
        else {
          $this->cart->applyDiscount($discountDetails['discountId'], $discountDetails['promocode'], $discountDetails['discount'], $discountDetails['discountType']);
          echo json_encode(array("status" => true, "message" => "apply"));
        }
      }
    }

  }
  public function getClientAddressbyID()
    {
      $id=$this->input->post('addressID');
      $this->load->model('orderModel');
      $result = $this->orderModel->getClientAddressbyID($id);
      $result = (array_key_exists(0, $result))? $result[0]: array();
      $result = (array_key_exists(0, $result))? $result[0]: array();

      echo json_encode($result);


    }
}
