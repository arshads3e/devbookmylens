<?php if ($locationKey=="BNGLR") { ?>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table width="750" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:calibri;" >

  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
    <!--investment plan start    -->
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:2px solid #ab2f2f; font-family:calibri;" >
  <tr><td>&nbsp;</td></tr>
   <tr>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center">
          <img src="<?php echo base_url(); ?>images/bml_logo_new.png" style="max-height:50px; margin-top:-5px;">
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr><td>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:10px;">
  <tr><td>&nbsp;</td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
  <td>
   <p style="font-size:18px;">Dear
      <?php
        if($firstName!="")
        {
           echo $firstName.", ";
        }
      ?>
  </p>
  <p>Thank you for signing up with BML. We would like to request you to verify your email by clicking on this link <a href = "<?php echo site_url('myaccount/confirmEmail/'.$email.'/'.$code);?>">click me</a>. </p>
  <p>We would also like to request you to complete the registration by uploading the following documents</p>
  <ol>
	<li>Passport Copy / Rental Agreement / Bank Statement (Latest)</li>
	<li>PAN + Aadhaar Card / DL / Company ID Card / Utility Bill</li>
	<li>Photo (PP Size)</li>
	<li>Attach the relevant documents mentioned. <b>Passport Copy is Mandatory</b></li>
	<li>Complete the registration form by providing all the details.<br>
		Incase the address in your passport does not match the current address, you will need to submit a copy of either rental agreement or utility bill (Telephone/Electricity)</li>
  </ol>
  <p>BookMylens will do further verification and approve/reject the same.</p>
  <p><b>Payment of Registration Amount:</b> <br>
		Customer can add the following account as beneficiary and Transfer an Registration Fee of 500 INR or Issue a cheque in favor of "Creative Capture Pvt Ltd" (Account will be processed in 1 Day, 500 INR non-refundable) 
		<ul>
			<li>Creative Capture Pvt Ltd </li>
			<li>Current Account No: 50200005019560</li>
			<li>Ifsc code : HDFC0000053</li>
			<li>HDFC Bank , KORAMANGALA </li>
		</ul>
		and send an email from his/her business email-id to rentals@bookmylens.com (Transaction Details / Cheque Number)</p>
  <p>&nbsp;</p>
  <p>You toil hard to create an image, now you can showcase it to the world in a clutter free, 
	high-resolution format on your own website. As part of the BookMyLens family, you are entitled 
	to an account on Justfolio where you can create a website in a matter of minutes. 
	Click http://www.justfolio.com/refer/BMYL15 to get started.</p>
  <p>&nbsp;</p>
  <p><b>Mandatory:</b> First time renters need to visit our office along with the original documents submitted for registration (For Verification Purpose).</p>
  
  <p>&nbsp;</p>
  <p>BookMyLens Team,<br>
	Call us: 080 41633669/ 9611234528<br>
	Email Id: rentals@bookmylens.com<br>
	www.bookmylens.com<br>
	www.facebook.com/bookmylens</p>
  </td>
  </tr>
    <tr><td>&nbsp;</td></tr>
  </table>
  </td></tr>
  <tr><td align="center">
  <p style="color:grey; font-size:12px;">This welcome email is sent to <a><ins style="color:#24798e"><?php echo $email; ?></ins></a> because you have signed up recently at <a href="http://bookmylens.com" style="color:#24798e">www.bookmylens.com</a>
</p>
<p style="margin-bottom:10px;"></p>
</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
    </td>
  </tr>
</table>
</td>
</tr>
</table>
</body>
<?php } else { ?>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table width="750" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:calibri;" >

  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
    <!--investment plan start    -->
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:2px solid #ab2f2f; font-family:calibri;" >
  <tr><td>&nbsp;</td></tr>
   <tr>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center">
          <img src="<?php echo base_url(); ?>images/bml_logo_new.png" style="max-height:50px; margin-top:-5px;">
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr><td>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:10px;">
  <tr><td>&nbsp;</td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
  <td>
   <p style="font-size:18px;">Dear
      <?php
        if($firstName!="")
        {
           echo $firstName.", ";
        }
      ?>
  </p>
  <p>Thank you for signing up with BML. We would like to request you to verify your email by clicking on this link <a href = "<?php echo site_url('myaccount/confirmEmail/'.$email.'/'.$code);?>">click me</a>. </p>
  <p>We would also like to request you to complete the registration by uploading the following documents</p>
  <ol>
    <li>Passport Copy / Rental Agreement / Bank Statement (Latest)</li>
	<li>PAN + Aadhaar Card / DL / Company ID Card / Utility Bill</li>
	<li>Photo (PP Size)</li>
	<li>Attach the relevant documents mentioned. <b>Passport Copy is Mandatory</b></li>
	<li>Complete the registration form by providing all the details.<br>
		Incase the address in your passport does not match the current address, you will need to submit a copy of either rental agreement or utility bill (Telephone/Electricity)</li>
  </ol>
  <p>BookMylens will do further verification and approve/reject the same.</p>
  <p><b>Payment of Registration Amount:</b> <br>
		Customer can add the following account as beneficiary and Transfer an Registration Fee of 500 INR or Issue a cheque in favor of "Creative Capture Pvt Ltd" (Account will be processed in 1 Day, 500 INR non-refundable) 
		<ul>
			<li>Creative Capture Pvt Ltd </li>
			<li>Current Account No: 50200027284789</li>
			<li>Ifsc code : HDFC0000065</li>
			<li>HDFC Bank , Mysore </li>
		</ul>
		and send an email from his/her business email-id to rentals_mysore@bookmylens.com  (Transaction Details / Cheque Number)</p>
  <p>&nbsp;</p>
  <p>You toil hard to create an image, now you can showcase it to the world in a clutter free, 
	high-resolution format on your own website. As part of the BookMyLens family, you are entitled 
	to an account on Justfolio where you can create a website in a matter of minutes. 
	Click http://www.justfolio.com/refer/BMYL15 to get started.</p>
  <p>&nbsp;</p>
  <p><b>Mandatory:</b> First time renters need to visit our office along with the original documents submitted for registration (For Verification Purpose).</p>
  
  <p>&nbsp;</p>
  <p>BookMyLens Team,<br>
	Call us: 080 41633669/ 9611234528<br>
	Email Id: rentals_mysore@bookmylens.com <br>
	www.bookmylens.com<br>
	www.facebook.com/bookmylens</p>
  </td>
  </tr>
    <tr><td>&nbsp;</td></tr>
  </table>
  </td></tr>
  <tr><td align="center">
  <p style="color:grey; font-size:12px;">This welcome email is sent to <a><ins style="color:#24798e"><?php echo $email; ?></ins></a> because you have signed up recently at <a href="http://bookmylens.com" style="color:#24798e">www.bookmylens.com</a>
</p>
<p style="margin-bottom:10px;"></p>
</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
    </td>
  </tr>
</table>
</td>
</tr>
</table>
</body>
<?php } ?>