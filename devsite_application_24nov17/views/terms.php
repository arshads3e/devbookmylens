<div class="clearfix"></div>
<br>

	
		<div class="col-sm-2 col-md-2"></div>
		<div class="col-sm-8 col-md-8">
			<h2 class="text-color"><strong>Terms and Conditions</strong></h2>
			
					<div class="text-justify padding-left-25px">

							<ol><b><li class="color-c1272d">Equipment</li></b><br>The Equipment remains at all times the sole and exclusive property of BookMyLens.
							The borrower has no rights or claims to the Equipment. BookMyLens does not have or make any claim to images or video made
							by the borrower while using the equipment. All equipment are tested before shipping. It is the borrower’s responsibility to
							check that the equipment is in good working order once borrower receives the equipment.
							<br>BookMyLens will not be responsible for any defects or deficiencies in the equipment unless immediate notification
							has been made. The delivery person will stay for 10 minutes so the customer can check the equipment for minor and major
							issues and make a note of the same on the delivery slip. Minor issues being scratch on the lens body or lens cap missing etc.,
							In case there is any major issue like IS not working or not able to take images using this lens etc., the lens can be returned
							to the delivery person.<br>By agreeing to our terms of rent the borrower accepts that should another person sign for the equipment
							on his/her  behalf then that person is authorised by borrower to carry out the above checks. It is the borrower’s responsibility
							to ensure the equipment on rent is suitable for the purposes for which the borrower requires. We do not allow rehire of our
							equipment to third parties and no cross hires of any kind are permitted.<br><br> <b><li class="color-c1272d">Loss or damage to equipment
							(Lost/Stolen/Damage)</li></b><br>The borrower shall be responsible for the safe keeping of the equipment
							[lens &amp; accessories as applicable] throughout the rent period and shall be liable to BookMyLens for all loss of or
							damage to the equipment howsoever caused. Borrower shall not materially modify or alter the Equipment. In the event of any
							material modifications, borrower will be responsible for all reasonable costs of BookMyLens in restoring the Equipment to
							its normal condition. The borrower shall be required to repackage the equipment as delivered at the time of return to BookMyLens.
							Please make sure to go through the checklist so that none of the [lens &amp; accessories as applicable] is missing.
							<br>Borrower shall notify BookMyLens for any loss or damage to the equipment on rent immediately if such loss or damage
							is sustained.<br>Borrower shall pay to BookMyLens all costs for repairs along with the daily rental charge until the equipment
							can be repaired or replaced.<br>If the equipment on rent are not returned after the rental tenure in time and if the borrower is
							not reachable via email/phone, BookMyLens will take it forward with the third party (security agency) for further investigation
							and recovery of the equipment.<br><br><b><li class="color-c1272d">Cancellation</li></b><br>If an order is cancelled by the
							borrower within 4 working days from the date when the period of rent was to commence, the borrower will be liable to pay
							BookMyLens a cancellation charge equal to and not exceeding the agreed rental charge.<br><br><b><li class="color-c1272d">Reservation</li></b>
							<br>Upon booking of equipment at least 4 days prior to the rental start date, by telephone or e-mail, BookMyLens shall
							block the equipment for 24 hrs during which the borrower needs to make an advance payment of 50% of the total rental
							amount or one day’s rental charge.<br>It is mandatory to provide an acceptable proof of identity through e-mail before the
							equipment is dispatched.<br><b> Important Note: </b> In the event of not receiving valid proof of identity and BookMyLens
							is unable to contact borrower [phone or email], the reservation will be cancelled without any further notice.<br><br>
							<b><li class="color-c1272d">Delivery</li></b><br> The lens will be sent so that it arrives in time for the beginning of borrower hire period
							this will normally be the day before Day1.<br> There are two delivery options for customers.<br><div class="padding-left-20px"><ol> <li>The lens will
							be sent through our authorized executive so that it arrives on the day before Day 1 of the borrower’s rental period.<br>
							</li><li> The borrower can come to the BookMylens office and collect the equipment one day prior to the start of the
							borrower’s rental period. (This is with a prior appointment with the BookMyLens Team).</li></ol></div><br>A typical
							1 Day rent works like this: Start date of the rental: Jan 10th 2012 <br> End date of the rental: Jan 10th 2012 <br>
							Equipment Pickup date: Jan 9th 2012   (Evening 5:00 PM to 6:30 PM)<br> Equipment Return date: Jan 11th 2012
							(Morning 9:30 AM to 11:00 AM)<br><br> <b><li class="color-c1272d">Collection</li></b>
							<br>The equipment needs to be personally returned to the BookMyLens office.
							All liability for the equipment remains with the hiring parties until the safe return of the equipment to BookMyLens.<br>
							<br> <b><li class="color-c1272d">Payment</li></b><br>Before any equipment is dispatched or personally collected at the BookMyLens office; 100%
							payment for the entire rental period must be made.<br><br><b> <li class="color-c1272d">Usage of equipment outside INDIA</li>
							</b> You must notify BookMyLens of your intention to take the equipment outside India and gain the permission to do so.
							You will declare the equipment with the customs prior to leaving India. You are responsible for any custom fee imposed
							on the equipment, if you fail to declare the equipments with the customs.</br></ol>


					</div>
			</div>

		</div>
		
	     <div class="col-sm-2 col-md-2"></div>