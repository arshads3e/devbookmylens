<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OrderModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getPaymentMaster()
    {
      $sql = "SELECT * FROM `payment_type_master` WHERE 1";
      return $this->bml_database->getResults($sql);
    }

    public function getPaymentDetails()
    {
      $customerId = $this->user_session->getSessionVar('customerId');
      $sql = "SELECT a.*, d.typeDisplayName, c.total, c.orderID
              FROM `order_payment` a, tbl_suborder b, ordertb c, payment_type_master d
              WHERE a.`orderID` = b.orderID
              and a.orderID = c.orderId
              and a.paymentType = d.typeID
              and c.customerId = '$customerId' order by b.subOrderID desc;
              SELECT ifnull(sum(a.amount), 0) as paid, GROUP_CONCAT(DISTINCT(b.`orderNumber`)) as orderNumbers, c.total, c.orderID
              FROM ordertb c
              JOIN tbl_suborder b
              ON b.orderID = c.orderId
              LEFT JOIN `order_payment` a
              ON a.orderID = c.orderId
              and a.status = 1
              where c.customerId = '$customerId'
              GROUP BY c.`orderId`  order by b.subOrderID desc;
            ";
      return $this->bml_database->getResults($sql);
    }

    public function getOrderDetails($orderID)
    {
      $sql = "SELECT a.`total`, a.`dicountAmount` FROM `ordertb` a WHERE a.`orderID` = ".$orderID.";\n";
      $sql .= "SELECT a.`startDate`, a.`endDate`, a.`collectDate`, a.`returnDate`, d.itemName, MAX(e.days), e.price as 'perDayPrice', a.`duration`,  a.`rentalTotalPrice`, a.`status`, a.`notes`, a.qty, b.orderNumber
              FROM `orderproduct` a
              JOIN tbl_suborder b
              on a.`subOrderID` = b.subOrderID
              JOIN itemmaster d
              on d.itemId = a.`itemId`
    		  JOIN pricemaster e
			  on d.itemId = e.itemId
			  and a.`duration` >= e.days
              WHERE b.orderID = $orderID
			  group by a.`orderProductId`;\n";
      return $this->bml_database->getResults($sql);
    }
	
    public function getSubOrderDetails($subOrderID)
    {
      $sql = "SELECT a.`orderSubtotal` as 'total', a.`orderDiscountAmount` as 'dicountAmount' FROM `tbl_suborder` a WHERE a.`subOrderID` = ".$subOrderID.";\n";
      $sql .= "   SELECT a.`startDate`, a.`endDate`, a.`collectDate`, a.`returnDate`, d.itemName, MAX(e.days), e.price as 'perDayPrice', a.`duration`,  a.`rentalTotalPrice`, a.`status`, a.`notes`, a.qty, b.orderNumber
				  FROM `orderproduct` a
				  JOIN tbl_suborder b
				  on a.`subOrderID` = b.subOrderID
				  JOIN itemmaster d
				  on d.itemId = a.`itemId`
				  JOIN pricemaster e
				  on d.itemId = e.itemId
				  and a.`duration` >= e.days
				  WHERE b.subOrderID = $subOrderID
				  group by a.`orderProductId`;\n";
		//echo $sql;		  
      return $this->bml_database->getResults($sql);
    }

    public function getClientAddress($clientID = -1,$locationKey)
    {
      
      $clientID = ($clientID == -1)? $this->user_session->getSessionVar('customerId'): $clientID;
      $sql = "SELECT * FROM `customer_address` WHERE `customerId` = '".$clientID."'";
      return $this->bml_database->getResults($sql);
      
    }
    public function getClientAddressbyID($id)
    {
      if($id==1)
      {
        $customerId='-1';
      }
      else if($id==2)
      {
        $customerId='-2';
      }
      else {
        $customerId = $this->user_session->getSessionVar('customerId');
      }
      $sql = "SELECT  `addressLine1`, `addressLine2`, `addressCity`, `addressState`, `addressPin`, `addressLandmark`,  `shippingCost`, `status` FROM `customer_address` WHERE `customerId` = '".$customerId."' and `addressID`='".$id."'";
      return $this->bml_database->getResults($sql);
    }

    public function getOrderCount()
    {
      $sql = "select count(1) as count from ordertb where customerId = '".$this->user_session->getSessionVar('customerId')."' and `status` = 7";
      return $this->bml_database->getResults($sql);
    }

    public function getAvailableQtyByItemId($itemID, $startDate = null, $endDate = null)
    {
      if($startDate == null)
      {
        $startDate = new DateTime();
      }
      else
      {
        $startDate = new DateTime($startDate);
      }
      if($endDate == null)
      {
          $endDate = new DateTime();
          $endDate->add(new DateInterval('P9M'));
      }
      else
      {
        $endDate = new DateTime($endDate);
      }
      $locationKey=$this->user_session->getSessionVar('locationKey');
     
      $sql = "call sp_get_order_product_qty($itemID, '".$startDate->format('Y-m-d')."', '".$endDate->format('Y-m-d')."','".$locationKey."')";
      return $this->bml_database->getResults($sql);
    }

    public function getHolidayList()
    {
      $sql ="SELECT `holidayDate` FROM `holidaylist`";
      return $this->bml_database->getResults($sql);
    }

    public function getDiscountDetails($discountCode)
    {
      $sql = "select *
              from discount
              where promocode = '$discountCode';
              SELECT count(1) as count
              FROM `ordertb` a
              JOIN discount b
              on a.`discountID` = b.discountId
              WHERE b.promocode = '$discountCode';";
      return $this->bml_database->getResults($sql);
    }
}
