
<div class="loadingModal"><!-- Place at bottom of page --></div>
<div class="clearfix"></div>
<footer style="background-color: rgb(230, 230, 230); height: 30px;">
	<div class="hide" style="color: rgb(193, 39, 45); margin-right: 30px; margin-top: 5px;" class="pull-right">
		Developed by <a target="_blank" href="http://joyphase.com">Joyphase</a>
	</div>
</footer>
<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.event.move.js?".version); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/responsive-slider.js?".version); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/jquery-ui.js?".version); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.validate.js?".version); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/Side-menu.js?".version); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js?".version); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/bml.js?".version); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.number.min.js?".version); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/dateselector.js?".version); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.flexisel.js?".version); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/jquery.quickfit.js?".version); ?>"></script>
<script src="<?php echo base_url(); ?>assets/dist/plugins/datatables/jquery.dataTables.min.js?<?php echo version; ?>" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/dist/plugins/datatables/dataTables.bootstrap.min.js?<?php echo version; ?>" type="text/javascript"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-30311055-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
