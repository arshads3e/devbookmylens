<div class="clearfix"></div>
<br>

	
		<div class="col-sm-2 col-md-2"></div>
		<div class="col-sm-8 col-md-8">
			<h2 class="text-color"><strong>Privacy Policy</strong></h2>
			<p class="privacy-title">Information Collected</p>
			<p class="text-justify policy-content">We collect information from you when you register on the site, reserve an item and all the communication such as e-mail. During the registration with our site, we will ask you for your address, enail and phone number. You may also browse our site anonymously. we use Google Analytics to track visitor trends. This information is anonymous and kept confidential.</p>
			<p class="privacy-title">Usage of information collected</p>
			<p class="text-justify policy-content">To quickly process your order. To track your order history so to answer any concern or disputes.</p>
			<p class="privacy-title">Protection of visitor information</p>
			<p class="text-justify policy-content">All kind of information collected from our customers is stored on secure servers with only limited number of people having the privilege to access it. These people will keep your information confidential.</p>
			<p class="privacy-title">Changes to our policy</p>
			<p class="text-justify policy-content">We reserve the right to alter our privacy policy at any time and without notice. Updates to the privacy policy will be posted on this site. This policy was last modified on June 26, 2012.</p>
			<p class="privacy-title">Questions and feedback</p>
			<p class="text-justify policy-content">Please contact us by email if you have any questions. The contact information may be found here.</p>
			<p class="privacy-title">Online Policy Only</p>
			<p class="text-justify policy-content">This online privacy policy applies only to information collected through our website and not to information collected offline.</p>
			<p class="privacy-title">Your consent</p>
			<p class="text-justify policy-content">By using our site, you consent to our privacy policy.</p>
		</div>
		
	     <div class="col-sm-2 col-md-2"></div>