          <div>
            <div>
              <div class="box">
                <div class="box-body">
                  <table id="productDetails" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <td>Discount ID</td>
                        <td>Discount</td>
                        <td>Discount count</td>
                        <td>Promo Code</td>
                        <td>CustomerIDS</td>
                        <td>Status</td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($tableRows as $row) { ?>
                        <tr>
                          <?php foreach ($row as $colName=>$cols) { ?>
                          <td>
                           <?php if($colName=='status') {?>
                           <?php
                             if($cols==1)
                             {
                              echo "ACTIVE";
                             }
                             else
                             {
                              echo "INACTIVE";
                             }

                           ?>
                           <?php } elseif ($colName=='numStock') { ?>
                             <?php echo "<div class='margin-left-25 margin-right-25'>$cols</div>";?>
                             <?php } elseif ($colName=='discountId') { ?>
                             <?php  $id=$cols;
                                    echo $cols;
                                  } else { ?>
                              <?php echo $cols;?>
                              <?php } ?>
                            </td>
                          <?php } ?>
                          <td><a href="<?php echo admin_url('discounts/editDiscountDetails/'.$id); ?>" class="btn btn-success btn-xs">EDIT</a></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
          $('#productDetails').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
      </script>
