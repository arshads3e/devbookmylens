<div class="box">
<div class="box-body">
  <div class="row">
    <div class="col-md-12 table-responsive">

      <h2>Order Summary</h2>
      <table class="table">
        <thead class="background-color-c1272d color-fff;">
          <th>Order Number</th>
          <th>Product Name</th>
          <th>Pickup Date</th>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Return Date</th>
          <th>Qty</th>
          <th>Price</th>
          <th>Sub-Total</th>
        </thead>
        <tbody>
        <?php foreach ($subOrderItems as $orderNumber => $orderItems) { $orderItemCount = count($orderItems);?>
          <tr class="border-top-2px-solid-C1272D" >
            <td rowspan="<?php echo $orderItemCount;?>" class="vertical-align-middle border-left-2px-solid-C1272D border-right-2px-solid-C1272D"><?php echo $orderNumber;?></td>
            <?php
              $subOrderTotal = 0;
              $first = true;
              $colTextArr = "";
              foreach ($orderItems as $orderItem) {
                $waitingList = ($orderItem['status'] == 6) ? "class='bg-danger' title='waitingList'":'';
                $colText = "";
                $subOrderTotal += $orderItem['rentalTotalPrice'];
                if(!$first) $colText.="</tr><tr>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".$orderItem['itemName']."</td>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".dateFromMysqlDate($orderItem['collectDate'])."</td>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".dateFromMysqlDate($orderItem['startDate'])."</td>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".dateFromMysqlDate($orderItem['endDate'])."</td>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".dateFromMysqlDate($orderItem['returnDate'])."</td>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".$orderItem['qty']."</td>";
                $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".$orderItem['rentalTotalPrice']."</td>";
                $colTextArr[] = $colText;
                $first = false;
              }
              $first = true;
              foreach ($colTextArr as $colText) {
                echo $colText;
                if($first) {
                  echo "<td rowspan='".$orderItemCount."' class='vertical-align-middle border-left-2px-solid-C1272D border-right-2px-solid-C1272D'>
                            ".$subOrderTotal."
                        </td>";
                  $first = false;
                }
              }
            ?>
          </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
