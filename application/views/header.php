<script type="text/javascript">

   className = "<?php echo $this->router->fetch_class();?>";

   methodName = "<?php echo $this->router->fetch_method();?>";

   site_url = "<?php echo site_url();?>";

</script>



  <!-- start of header -->

  <div class="modal-header">



		<div class="row">

			<div class="col-md-2">

				<!-- 1st col -->

				<a href="<?php echo site_url('welcome');?>">

				<img src="<?php echo site_url('images/bml_logo_tm.png');?>" class="logo height-80px width-130px">

				</a>

			</div>





			<form name="header" method="post" class="form-horizontal">

				<div class="col-md-8 margin-top-20px" >

					<div class="row">

						<div class="col-lg-6"></div>



						<div class="col-lg-6">

							<span id="tagline">Premium Photography Equipment Rental Service</span>

						</div>



					</div>

					<div class="row">

						<div class="col-md-11">

							<div class="input-group">

								<div class="input-group-btn">

									<button type="button" class="btn btn-default dropdown-toggle margin-left-25px"

										id="check" data-toggle="dropdown" >

										<img src="<?php echo site_url('images/bml_logo.PNG') ?>" align="absmiddle"

											class="margin-left-8px">

										<span class="caret"></span>

									</button>

								</div>

								<input type="text" id="searchBox" name="searchBox"

									class="form-control input-lg" placeholder="Search Here">

							</div>

						</div>

					</div>

				</div>

			</form>



			<div class="col-md-2 text-center">

				<div id="call">CALL US NOW</div>
				
				<div id="phoneNo">1800-121-0446</div>

				<div id="timing">9:30AM - 6:30PM / Sunday Holiday</div>

			</div>

		</div>

	</div>

	<div class="bs-example navbar-default background-c1272d">



		<div class="navbar-header">

			<button class="navbar-toggle" type="button" data-toggle="collapse"

				data-target=".js-navbar-collapse">

				<span class="sr-only">Toggle navigation</span> <span

					class="icon-bar"></span> <span class="icon-bar"></span> <span

					class="icon-bar"></span>

			</button>



		</div>



		<div class="collapse navbar-collapse js-navbar-collapse">

			<ul class="nav navbar-nav border-radius-0px">

				<li class="dropdown">

					<a class="dropdown-toggle color-fff" href="#"

					   data-toggle="dropdown">Rental Equipments

					   <img src="<?php echo site_url('images/req_equip1.png');?>">

				    </a>

    				<ul class="dropdown-menu margin-top-0px border-radius-0px padding--1px padding-bottom-0px height-auto width-217px background-c1272d margin-left-15px">

                       	<div class="sidebar">

							<ul>

								<?php foreach ($category as $category_name => $category_row) { ?>



								<li><a href="javascript:void(0)"><?php echo $category_name; ?>

										<i class="sidebar-icon glyphicon glyphicon-chevron-down">

										</i>

									</a>

									<ul class="sub-menu">

										<a href="<?php echo site_url('productlist/category/'.urlencode($category_name));?>"><li><a href="<?php echo site_url('productlist/category/'.urlencode($category_name));?>">View All</a>

										</li></a>





										<?php foreach ($category_row as $brand) { ?>

                      <li>

                        <a href="<?php echo site_url('productlist/brand/'.urlencode($category_name).'/'.urlencode($brand));?>">

                          <?PHP echo $brand; ?>

                        </a>

										  </li>

										<?php } ?>

									</ul>

								</li>

								<?php } ?>

							</ul>

						</div>

					</ul>

				</li>



				<li><a href="<?php echo site_url('productlist/featured'); ?>" class="color-fff">Featured Equipments </a></li>



				<li><a href="<?php echo site_url('howorders'); ?>" target="_BLANK" class="color-fff">How To Order</a></li>

				<li id=""><a href="http://gallery.bookmylens.com/" target="_BLANK" class="color-fff">Gallery</a></li>

				<li><a href="<?php echo site_url('contactus'); ?>" target="_BLANK" class="color-fff">Contact Us</a></li>

                <li class="dropdown">

                	<a href="#" data-toggle="dropdown" class="dropdown-toggle color-fff">Learn More <b

						class="caret"></b>

					</a>

					<ul class="dropdown-menu color-fff border-radius-0px background-74171b margin-top-0px">

						<li><a href="http://blog.bookmylens.com/" target="_BLANK" class="color-fff">Blog</a></li>

						<li class="divider" class="background-d74a28"></li>

						<li><a href="<?php echo site_url('faq'); ?>" target="_BLANK" class="color-fff">FAQ</a></li>

						<li class="divider" class="background-d74a28"></li>

						<li><a href="<?php echo site_url('aboutus'); ?>" target="_BLANK" class="color-fff">About</a></li>

						<li class="divider" class="background-d74a28"></li>

						<li><a href="<?php echo site_url('services'); ?>" class="color-fff">Services</a></li>

						<li class="divider" class="background-d74a28"></li>

						<li><a href="<?php echo site_url('terms'); ?>" target="_BLANK" class="color-fff">Terms</a></li>

					</ul>

				</li>

			</ul>

			<ul class="nav navbar-nav navbar-right">

            <?php if($this->user_session->isLoggedIn())

            {

               ?> <li class="dropdown">

                  <a href="#" data-toggle="dropdown" class="dropdown-toggle color-fff" ><i class="fa fa-user"></i> <?php echo $this->user_session->getSessionVar('firstName'); ?><b class="caret"></b></a>

               <ul class="dropdown-menu border-radius-0px background-74171b color-fff margin-top-0px">

                  <li><a href="<?php echo site_url('myaccount');?>" class="color-fff">Account Info</a></li>

                  <li class="divider" class="background-d74a28"></li>

                  <li><a href="<?php echo site_url('myaccount/logout'); ?>" class="color-fff">Log out</a></li>



               </ul>

            </li>

            <li class="dropdown">

                  <a href="#" data-toggle="dropdown" class="dropdown-toggle color-fff" >View Cart <i class="fa fa-cart-arrow-down"></i> <b class="caret"></b></a>

               <ul class="dropdown-menu border-radius-0px background-74171b color-fff margin-top-0px">

                  <?php if(count($this->cart->contents()) == 0) {?>

                     <li>

                        <a href="#" class="color-fff">

                           No Items In Cart

                        </a>

                     </li>

                     <li class="divider" class="background-d74a28"></li>



                  <?php } else {?>

                     <?php foreach ($this->cart->contents() as $items): ?>

                        <li>

                           <a href="#" class="color-fff">

                              <span class="trash" onclick="removeCartProduct('<?php echo $items['rowid'];?>')">

                                 <i class="fa fa-trash-o"></i>

                              </span>

                              <?php echo character_limiter($items['name'], 10);?>

                              <span class="cartprice" class="float-right">

                                 <i class="fa fa-inr"></i>

                                 <?php echo $this->cart->format_number($items['subtotal']);?>

                              </span>

                           </a>

                        </li>

                        <li class="divider" class="background-d74a28"></li>

                     <?php endforeach; ?>

                     <li class="divider" class="background-d74a28"></li>

                     <li><a href="#" class="color-fff">

                        <span class="trash">&nbsp;&nbsp;</span>

                        Total Rent

                        <span class="cartprice" class="float-right">

                           <i class="fa fa-inr"></i>

                           <?php echo $this->cart->format_number($this->cart->total()); ?>

                        </span>

                     </a></li>

                     <li class="divider" class="background-d74a28"></li>

                     <li><a href="#" class="color-fff">

                        <span class="trash">

                           &nbsp;&nbsp;

                        </span>

                        Discount Applied

                        <span class="cartprice" class="float-right">

                           <i class="fa fa-inr"></i>

                           0

                        </span>

                     </a></li>

                     <li class="divider" class="background-d74a28"></li>

                     <li><a href="#" class="background-EC9817 color-000">

                        <b>

                           <span class="trash">&nbsp;&nbsp;</span> Grand Total  &nbsp;&nbsp;&nbsp;&nbsp;

                           <span class="cartprice">

                              <i class="fa fa-inr"></i>

                              <?php echo $this->cart->format_number($this->cart->total()); ?>

                           </span>

                        </b>

                     </a></li>

                     <form id="promocodeForm" method="post" action ="<?php echo site_url('cart/applyDiscount');?>">

                           <div class="prompading"><input type="text" class="color-000" onclick="event.preventDefault();" name="coupon" placeholder="use promo code here" required></div>

                           <input type="hidden" name="discountAction" value="apply">

                           <div class="text-center couponError"><span class="text-danger text-center" id="couponError"></span></div>

                           <div class="prompading text-center"><button type="submit" class="promocode btn btn-warning btn-sm"> Use Promo Code </button></div>

                     </form>

                     <div class="text-center">

                        <a href="<?php echo site_url('cart/view');?>" class="btn btn-primary"> View Cart </a>

                        <a href="<?php echo site_url('cart/checkout');?>" class="btn btn-success"> Checkout </a>

                     </div>



                  <?php } ?>

               </ul>

            </li>

             <?php } else {?>

				<li class="dropdown dropdown-large">

         			<a href="#signupModal" class="dropdown-toggle color-fff" data-toggle="modal" >Log In/Sign Up </a>

         			<div id="signupModal" class="modal fade">

         				<div class="modal-dialog" >

         					<div class="modal-content">

         						<div class="modal-header background-color-fff border-bottom-none" >

         							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

         						</div>

         						<div class="modal-body background-color-fff">

         						<div class="col-md-6 col-sm-6" >

         						<form action="<?php echo site_url('myaccount/login');?>" id ="loginForm" name="loginForm" id="loginForm"  class="form-inline" method="post" role="form" >

         							<div class="row" >



         										<span class="fontDesign">Login</span>

         										<div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">

         											<input type="email"  name="username" id="username" class="form-control bordervalue width-100" id="inputEmail3" placeholder="Email" value="" required>

         										</div>

         										<div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">

         											<input type="password" name="password" id="password" class="form-control bordervalue width-100"  value="" placeholder="Password"  required>

         										</div><div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">

         											<input type="submit" name="SubmitLogin" class="btn btn-danger btn-md width-100" value="Login"  >

         										</div>

         										 <div  class="formAlign">

         										
         										<a href="#forgotpassword" class="dropdown-toggle" data-toggle="modal"><label class="forgotDesign"  data-dismiss="modal">Forgot Password</label></a>
												
         										</div>

         								</div>



         						</form>

         						<h5 class="text-color text-center margin-top-50px"><strong>OR</strong></h5>

         						<div class="margin-top-50px">

         							<a href="<?php echo $this->facebook->login_url(); ?>">

         								<button type="button" class="btn btn-primary btn-sm btn-block">

         								    <img src="<?php echo site_url('images/loginfb.PNG'); ?>" align="left" class="margin-left-0px">

         								     <span class="">Login with Facebook</span>

         								 </button>

         							</a>

         						</div>



         				</div>

         					<div class="col-md-6 col-sm-6" >

         						<div class="visible-xs">

         							<hr>

         						</div>

         						<div class="vr hr1 background-a3a3a3" ></div>

         						<div  class="formAlign">

         							<span class="fontDesign">Register Now</span>

         						</div>

         						<!-- <form name="joinnowForm"  id="joinnowForm" method="post" class="form-inline"  role="form" action="<?php //echo site_url('myaccount/register'); ?>"> -->

         							 <form name="joinnowForm" id="joinnowForm" method="post" class="form-inline"  role="form" action="<?php echo site_url('myaccount/register'); ?>"> 

									 <div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">

         								<input type="text" name="fname" id="fname" placeholder="First Name"   class="form-control bordervalue width-100" required>

         							</div>

         							<div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px" >

         								<input type="text" name="lname" id="lname" placeholder="Last Name" class="form-control bordervalue width-100">

         							</div>

                              <div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px" >

                                 Male : <input type="radio" name="gender" class="form-control bordervalue" value="M">

                                 Female : <input type="radio" name="gender" class="form-control bordervalue" value="F">

                             </div>



                            <div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px" >

                              <b>Registered as a:</b>

                              <div class="clearfix"></div>

                                 Individual : <input type="radio" name="registeredtype" class="form-control bordervalue" value="I">

                                 Company : <input type="radio" name="registeredtype" class="form-control bordervalue" value="C">

                            </div>

         							<div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">

         								<input type="text" name="emailId" id="emailId"  placeholder="Email Address" class="form-control bordervalue width-100" required>

         							</div>

         							<div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">

         								<input type="password" name="pwd1" id="pwd1" placeholder="Password(6 or more character)" class="form-control bordervalue width-100" required>

         							</div>

         							<div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">

         								<input type="password" name="pwd2" id="pwd2" placeholder="Confirm Password(6 or more character)" class="form-control bordervalue width-100" required>

         							</div>

         							<div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">

         								<input type="text" name="phone" id="Phone" placeholder="Contact Number" class="form-control bordervalue width-100" required>

         							</div>

         							<div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px" >

         								<input type="submit" name="joinnow" id="joinnow"  value="Join Now" class="btn btn-danger width-100"  >

         							</div>

         						</form>

         						<div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">

         							<p class="text-center font-size-10px font-weight-bold font-family-helvetica">By clicking Join Now,you agree to BookMyLens

         								<a href="<?php echo site_url('terms'); ?>" class="color-c53332">Terms </a> and

         								<a href="<?php echo site_url('privacy'); ?>" class="color-c53332">Privacy Policy</a>

         							</p>

         						</div>

         					</div>



         					</div>

         					<div class="clearfix"></div>

         				</div><!-- end login with fb form -->

         				</div><!-- end of login form container -->

         			</div><!-- end of ajax functionalityr -->

         		</div><!-- /.modal-content -->

         	</div><!-- /.modal-dialog -->

         </div><!-- /.modal -->

     </li>

     <?php } ?>

 </ul>

</div>

</div>



                  <div id="forgotpassword" class="modal fade">

         				<div class="modal-dialog" >

         					<div class="modal-content">

         						<div class="modal-header background-color-fff border-bottom-none" >

         							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

         						</div>

         						<div class="modal-body background-color-fff" >

         							<div class="col-sm-3 col-md-3"></div>

         						<div class="col-md-6 col-sm-6" >

         						<form name="forgotPassForm" action="<?php echo site_url('myaccount/forgotPassword');?>" id="forgotPassForm" class="form-inline" method="post" role="form" >

         							<div id="checkheader"></div>

         							<div id="logintoForgotpwd"></div>

         								<div class="row" >



         										<h5 class="fontDesign text-center">Forgot Password</h5>

         										<div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">

         											<input type="email" required name="email" id="email" class="form-control bordervalue width-100" placeholder="Enter your email-id" value=""  >

         										</div>



         										<div  class="col-lg-12 col-sm-12 col-md-12 margin-top-5px">

         											<input type="submit" name="SubmitLogin" class="btn btn-danger btn-md width-100" value="Reset Password">

         										</div>

         										 <div  class="formAlign " >

         											

         											<a href="#signupModal" class="dropdown-toggle" data-toggle="modal"><label class="forgotDesign margin-top-15px"   data-dismiss="modal">Back to Login</label></a>

         										</div>

         								</div>



         						</form>



         					</div>

         					<div class="col-sm-3 col-md-3"></div>

         					<div class="clearfix"></div>

         				</div><!-- end login with fb form -->

         				</div><!-- end of login form container -->

         			</div>

         		</div>

            <script type="text/javascript">

              myaccountURL = '<?php echo site_url('myaccount');?>';

			  adminURL = '<?php echo admin_url('home');?>';

			  smsVerification='<?php echo site_url('smsVerification');?>';

			  

             

            </script>



