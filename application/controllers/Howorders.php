<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Howorders extends CI_Controller {


	public function index()
	{
		$this->load->view('head');
		$category = $this->bml_read_json->readRentMenu();
		$data['category'] = $category;
		$this->load->view('header',$data);
		$this->load->view('howorders');
		$this->load->view('footer');
	}
	public function app()
	{
		$this->load->view('head');
		$this->load->view('howorders');
	}
}
