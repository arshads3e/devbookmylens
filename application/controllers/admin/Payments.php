<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {

  public function __construct()
  {
       parent::__construct();
    if(!$this->session->userdata('is_admin_login'))
    {
      redirect(site_url());
    }
    $this->load->model('adminModel');
    $this->load->model('orderModel');
    $this->load->helper('bml_util');
 }

 public function approve($paymentID)
 {
    $adminID = $this->user_session->getSessionVar('customerId');
    $sql  = "UPDATE `order_payment` SET `status`=1,`adminID` = $adminID WHERE `paymentID` = $paymentID";
    $this->bml_database->getResults($sql);
    header('Location: '.$this->agent->referrer());
 }

  public function addNew()
  {
    $data = array("title" => "Payments", "subTitle" => "Add New","sidebarCollapse" => true);
    $paymentMaster = $this->orderModel->getPaymentMaster();
    $paymentMaster = (array_key_exists(0, $paymentMaster))? $paymentMaster[0]: array();
    $paymentOptions = '';
    foreach ($paymentMaster as $paymentMasterRow) {
      $paymentOptions .= "<option value='".$paymentMasterRow['typeID']."'>".$paymentMasterRow['typeDisplayName']."</option>";
    }
    $data['paymentOptions'] = $paymentOptions;

    $paymentRestult = $this->adminModel->getPaymentDetails();
    $paymentRestult1 = (array_key_exists(0, $paymentRestult))? $paymentRestult[0]: array();
    $paymentRestult2 = (array_key_exists(1, $paymentRestult))? $paymentRestult[1]: array();

    $orderNumbersOption = '';
    foreach ($paymentRestult2 as $paymentRow) {
      $paymentRow['pending'] = $paymentRow['total'] - $paymentRow['paid'];
      if($paymentRow['pending'] > 0)
      {
        $orderNumbersOption .= "<option value='".$paymentRow['orderID']."'>".$paymentRow['orderNumbers']."</option>";
      }
    }
    $data['orderNumbersOption'] = $orderNumbersOption;

    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/paymentNew');
    $this->load->view('admin/footer');
  }
  public function view()
  {
    $data = array("title" => "Payments", "subTitle" => "View All","sidebarCollapse" => true);
    $paymentMaster = $this->orderModel->getPaymentMaster();
    $paymentMaster = (array_key_exists(0, $paymentMaster))? $paymentMaster[0]: array();
    $paymentOptions = '';
    foreach ($paymentMaster as $paymentMasterRow) {
      $paymentOptions .= "<option value='".$paymentMasterRow['typeID']."'>".$paymentMasterRow['typeDisplayName']."</option>";
    }
    $data['paymentOptions'] = $paymentOptions;

    $paymentRestult = $this->adminModel->getPaymentDetails();
    $paymentRestult1 = (array_key_exists(0, $paymentRestult))? $paymentRestult[0]: array();
    $paymentRestult2 = (array_key_exists(1, $paymentRestult))? $paymentRestult[1]: array();

    $orderNumbersOption = '';
    $paymentDetails = [];
    foreach ($paymentRestult2 as $paymentRow) {
      $paymentRow['pending'] = $paymentRow['total'] - $paymentRow['paid'];
      $paymentDetails[$paymentRow['orderID']] = $paymentRow;
      if($paymentRow['pending'] > 0)
      {
        $orderNumbersOption .= "<option value='".$paymentRow['orderID']."'>".$paymentRow['orderNumbers']."</option>";
      }
    }
    foreach ($paymentRestult1 as $paymentRow) {
      $paymentDetails[$paymentRow['orderID']]['details'][] = $paymentRow;
    }
    $data['orderNumbersOption'] = $orderNumbersOption;
    $data['paymentDetails'] = $paymentDetails;

    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/paymentList');
    $this->load->view('admin/footer');
  }
}
